.. _testing:

=======
Testing
=======


Tox
===

`Tox <https://tox.readthedocs.io/>`_ automation project is used to manage the
test environment and run additional quality control tools against the codebase.

The complete testing and quality control suite can be run using:

    .. code-block:: bash

        fab tox


Test suite
----------

The CLINK test suite can be run independently from the rest of the tools using:

    .. code-block:: bash

        fab test


Quality control
===============

In addition to the actual test suite, the following quality control tools are
run to ensure code passes quality control checks.


Coverage
--------

`Coverage <https://coverage.readthedocs.io/>`_ measures code coverage during
test execution, and produces reports. Tox is configured to run coverage
whenever the test suite is run.

A summary report is shown in the console when the tests are run, and a detailed
html report is saved to ``/htmlcov/`` in the project root.


Bandit
------
`Bandit <https://bandit.readthedocs.io/>`_ finds common security issues in
Python code.

Bandit can be run independently from the rest of the testing tools using:

    .. code-block:: bash

        fab tox -e bandit


Black
-----

`Black <https://black.readthedocs.io/>`_ is an opinionated and uncompromising
Python code formatter.

Black can be run independently from the rest of the testing tools using:

    .. code-block:: bash

        fab tox -e black


Docs
----

`Sphinx <https://www.sphinx-doc.org/>`_ is used to generate the project
documentation.

The project documentation can be generated independently from the rest of the
testing tools using:

    .. code-block:: bash

        fab docs


Flake8
------

`Flake8 <http://flake8.pycqa.org/>`_ is a python code linter, wrapping several
other utilities.

Flake8 can be run independently from the rest of the testing tools using:

    .. code-block:: bash

        fab tox -e flake8


isort
-----

`isort <https://github.com/timothycrosley/isort>`_ ensures consistency with
python import style and ordering (which is especially helpful when viewing
diffs).

isort can be run independently from the rest of the testing tools using:

    .. code-block:: bash

        fab tox -e isort


Mypy
----

`Mypy <https://mypy.readthedocs.io/>`_ is a static type checker.

Mypy can be run independently from the rest of the testing tools using:

    .. code-block:: bash

        fab tox -e mypy


Safety
------

`Safety <https://pyup.io/safety/>`_ checks installed dependencies for known
security vulnerabilities.

Safety can be run independently from the rest of the testing tools using:

    .. code-block:: bash

        fab tox -e safety
