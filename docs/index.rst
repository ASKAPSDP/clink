=====================
CLINK: Component Link
=====================

.. toctree::
   :maxdepth: 2
   :hidden:

   getting-started.rst
   consumers.rst
   producers.rst
   cli.rst
   history.rst
   development.rst
   testing.rst
   reference/clink.rst


CLINK is a Python library designed to make it easy to build
`CloudEvent <https://cloudevents.io/>`_ consumers and producers
(CLINK participants). It uses `Kombu <https://kombu.readthedocs.io/>`_ and
`AMQP <https://amqp.org/>`_ for messaging.


CLINK supports Python 3.6, 3.7, 3.8, 3.9, and 3.10.


Installation
============

Install from the
`Python Package Index, (pypi) <https://pypi.org/project/clink/>`_, using
`pip <https://pip.pypa.io/en/stable/>`_:

.. code-block:: bash

    python -m pip install clink


See :ref:`getting-started` for some first steps.


==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
