.. _commands:

========
Commands
========


A clink command-line interface is included to assist in development/testing.


.. code::

    clink [command]



Consume
=======

.. data:: consume

    Consume clink events.

    .. code-block:: text
        :emphasize-lines: 1

        Usage: clink consume




Emit an event
=============

.. data:: emit-event

    Emit clink events.

    .. code-block:: text
        :emphasize-lines: 1

        Usage: clink emit-event
