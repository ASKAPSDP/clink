.. _getting-started:

===============
Getting Started
===============


------------------------
Configure AMQP Transport
------------------------

Set the ``CLINK_TRANSPORT_URL`` environment variable:

.. code-block:: bash

    CLINK_TRANSPORT_URL=amqp://username:password@rabbitmq.example.com:5672//


------------
Participants
------------

A :py:class:`~clink.api.Participant` instance is the primary way in which your
code will interface with CLINK. It is suggested to create one participant
instance and use it across your project, or possibly one per submodule in a
larger project.

.. code-block:: python
    :name: participant-instance
    :emphasize-lines: 5

    from clink import clink


    # A participant can be a producer and/or a consumer
    participant = clink.Participant("unique.source.name")


Emitting an event (producer)
----------------------------

A participant may be a producer, and emit an
:py:class:`~clink.messaging.events.Event`. An event ``type`` must always be
provided, and you'll usually want to provide an event ``subject`` and ``data``
too.

.. code-block:: python
    :name: participant-producer
    :emphasize-lines: 7-11

    from clink import clink


    participant = clink.Participant("unique.source.name")


    participant.emit_event(
        type="com.example.clink.test_one",
        subject="test",
        data={"KEY": "VALUE"},
    )


See :ref:`producers` for more details.


Receiving events (consumer)
---------------------------

A participant may be a consumer, and receive events:

.. code-block:: python
    :name: participant-consumer
    :emphasize-lines: 13

    import logging
    import typing

    from clink import clink


    logger = logging.getLogger(__name__)


    participant = clink.Participant("unique.source.name")


    @participant.on_event("com.example.clink.test_one")
    def do_something(event: clink.Event, **kwargs: typing.Any) -> None:
        logger.info(
            f'Received event "{event.type}" for "{event.subject}" from '
            f'"{event.source}" at {event.time}.'
        )

        # TODO: Do something else with the event
        # ...


    if __name__ == "__main__":
        logging.basicConfig(level=logging.INFO)
        participant.consume()


See :ref:`consumers` for more details.


Receiving and emitting events (consumer & producer)
---------------------------------------------------

A participant may be a consumer and a producer, receiving and emitting events:

.. code-block:: python
    :name: participant-consumer-producer
    :emphasize-lines: 13-15,24-29

    import logging
    import typing

    from clink import clink


    logger = logging.getLogger(__name__)


    participant = clink.Participant("unique.source.name")


    @participant.on_event(
        "com.example.clink.test_one", "com.example.clink.test_two"
    )
    def do_something(event: clink.Event, **kwargs: typing.Any) -> None:
        logger.info(
            f'Received event "{event.type}" for "{event.subject}" from '
            f'"{event.source}" at {event.time}.'
        )

        # Emit another event (producer). Pass in the preceding event for
        # the trace_parent and trace_state values to be set.
        participant.emit_event(
            type="com.example.clink.test_three",
            subject="test",
            data={"KEY": "VALUE"},
            preceding_event=event
        )


    if __name__ == "__main__":
        logging.basicConfig(level=logging.INFO)
        participant.consume()


See :ref:`consumers` and :ref:`producers` for more details.
