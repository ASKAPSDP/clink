.. _development:

===========
Development
===========


Development Environment
=======================

Docker (compose) is used both for development and for GitLab CI to ensure a
consistent environment.


Get started by cloning the repository and work on the ``develop`` branch:

.. code-block:: bash

    git clone --branch=develop ssh://git@bitbucket.csiro.au:7999/askapsdp/clink.git

Copy the ``env.template`` file to ``.env`` for local customisation:

.. code-block:: bash

    cp env.template .env


Build services
--------------

``docker``, ``docker-compose``, and ``fabric`` (v2+) should be installed and
available locally to build/run the containers. On Ubuntu 20.04, you can install
these with:

.. code-block:: bash

    sudo apt install docker docker-compose fabric

To build the docker containers:

.. code-block:: bash

    fab build


Develop and test
----------------

Remember to run the tests locally when developing to catch failures before CI
is run on push to the repo.

Run tests:

.. code-block:: bash

    fab tox


Other commands
--------------

Generate docs:

.. code-block:: bash

    fab docs

Python shell:

.. code-block:: bash

    fab shell
