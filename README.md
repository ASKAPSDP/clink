# Component Link

[![pipeline status](https://gitlab.com/ASKAPSDP/clink/badges/develop/pipeline.svg)](https://gitlab.com/ASKAPSDP/clink/commits/develop) [![coverage](https://gitlab.com/ASKAPSDP/clink/badges/develop/coverage.svg)](https://askapsdp.gitlab.io/clink/coverage/)


Documentation: <https://askapsdp.gitlab.io/clink/>


CLINK is a Python library designed to make it easy to build
[CloudEvent](https://cloudevents.io/) consumers and producers (CLINK
participants). It uses [Kombu](https://kombu.readthedocs.io/) and
[AMQP](https://amqp.org/) for messaging.


CLINK supports Python 3.6, 3.7, 3.8, 3.9, 3.10, and 3.11.


## Installation

Install from the
[Python Package Index, (pypi)](https://pypi.org/project/clink/), using
[pip](https://pip.pypa.io/en/stable/):

```bash
python -m pip install clink
```
