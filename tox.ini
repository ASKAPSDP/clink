[tox]
download = true
envlist =
    python{3.6,3.7,3.8,3.9,3.10,3.11,3.12,3.13}
    check-manifest
    codespell
    coverage
    bandit
    black
    docs
    flake8
    interrogate
    isort
    mypy
    pip-audit
    pyroma
    ruff
    wheel
isolated_build = true
requires = virtualenv<20.22.0

[testenv]
allowlist_externals =
    /usr/local/bin/uv
basepython =
    .package: python3.6
    python3.6: python3.6
    python3.7: python3.7
    python3.8: python3.8
    python3.9: python3.9
    python3.10: python3.10
    python3.11: python3.11
    python3.12: python3.12
    python3.13: python3.13
commands =
    python -B -I -W once::DeprecationWarning -W error::RuntimeWarning -m coverage run --context={envname} -m xmlrunner --output=./xunit/ {posargs:discover}
extras =
    tests
install_command =
    uv --cache-dir=/var/cache/uv pip install --python={env_python} {opts} {packages}
package = wheel
setenv =
    CLINK_BACKEND = clink.backends.dummy
    PYTHONPATH = {toxinidir}
    PYTHONDONTWRITEBYTECODE = 1
wheel_build_env = .pkg

[testenv:python3.6]
deps =
    pip>=21.1
install_command =
    python -m pip install --cache-dir=/var/cache/pip {opts} {packages}


[testenv:bandit]
basepython =
    python3
commands =
    bandit -r docs src tests
deps =
    bandit
extras =
skip_install =
    true

[testenv:black]
basepython =
    python3
commands =
    black --check --diff docs fabfile.py src tests
deps =
    black>=24,<25
extras =
skip_install =
    true

[testenv:check-manifest]
basepython =
    python3
commands =
    check-manifest --verbose
deps =
    check-manifest
extras =
skip_install =
    true

[testenv:codespell]
basepython =
    python3
commands =
    codespell --summary --skip=docs/_build docs fabfile.py src tests
deps =
    codespell[toml]
extras =
skip_install =
    true

[testenv:coverage]
basepython =
    python3
commands =
    {env_python} -m coverage combine
    - {env_python} -m coverage lcov
    - {env_python} -m coverage xml
    - {env_python} -m coverage html
    {env_python} -m coverage report
depends =
    python{3.6,3.7,3.8,3.9,3.10,3.11,3.12,3.13}
extras =
    tests
parallel_show_output =
    true

[testenv:docs]
basepython =
    python3.10
commands =
    sphinx-apidoc --force --implicit-namespaces --module-first --no-toc --separate -o docs/reference src/clink
    sphinx-build -b html docs docs/_build
extras =
    docs

[testenv:flake8]
basepython =
    python3
commands =
    flake8 docs fabfile.py src tests
deps =
    flake8
    flake8-bugbear
    flake8-pyproject
extras =
skip_install =
    true

[testenv:interrogate]
basepython =
    python3
commands =
    interrogate --verbose docs fabfile.py src tests
deps =
    interrogate
extras =
skip_install =
    true

[testenv:isort]
basepython =
    python3
commands =
    isort --check-only --diff docs fabfile.py src tests
deps =
    isort[pyproject]>=5.10
extras =
skip_install =
    true

[testenv:mypy]
basepython =
    python3
commands =
    mypy --package clink
    mypy docs fabfile.py tests
extras =
    {[testenv]extras}
    types

[testenv:pip-audit]
basepython =
    python3
commands =
    pip-audit --desc --ignore-vuln=PYSEC-2023-228
deps =
    pip-audit

[testenv:pyroma]
basepython =
    python3
commands =
    pyroma .
deps =
    pyroma
extras =
skip_install =
    true

[testenv:ruff]
basepython =
    python3
commands =
    ruff check --diff --target-version=py37 docs fabfile.py scripts src tests
deps =
    ruff
extras =
skip_install =
    true

[testenv:wheel]
allowlist_externals =
    {[testenv]allowlist_externals}
    /usr/bin/rm
basepython =
    python3
commands =
    {env_python} -m build --outdir={envtmpdir} --wheel
    rm -rf ./build
    {env_python} -m twine check --strict {envtmpdir}/*.whl
    check-wheel-contents {envtmpdir}
deps =
    check-wheel-contents
    build
    twine
    wheel
extras =
skip_install =
    true
