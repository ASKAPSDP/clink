"""Fabric tasks."""

from __future__ import annotations

import os
import typing  # noqa: F401

import invoke


try:
    from fabric import api  # noqa: F401  # Test for fabric v1.x
except ImportError:
    # No fabric v1, so use primary module name
    import fabric
else:
    # Fabric v1 exists, so attempt to use fallback module name
    import fabric2 as fabric


gid = max(os.getgid(), 1000)

local_pwd = os.path.realpath(os.path.dirname(os.path.realpath(__file__)))

project_name = os.path.split(local_pwd)[-1]

uid = max(os.getuid(), 1000)


@fabric.task  # type: ignore[misc]
def bash(context, service="clink", command=""):
    # type: (invoke.context.Context, str, str) -> None
    """Launch bash shell in project container."""
    run(context, service=service, command=command, options="--entrypoint=bash")


@fabric.task  # type: ignore[misc]
def build(context, options=""):
    # type: (invoke.context.Context, str) -> None
    """Build container image."""
    with context.cd("."):
        invoke.run(
            "DOCKER_BUILDKIT=1 docker compose build "
            f"--build-arg uid={uid} --build-arg gid={gid} "
            f"{options} clink"
        )


@fabric.task  # type: ignore[misc]
def docs(context):
    # type: (invoke.context.Context) -> None
    """Build project documentation."""
    with context.cd("."):
        invoke.run("rm -rf ./docs/_build ./docs/reference")
    tox(context, envlist="docs")
    with context.cd("."):
        invoke.run(
            "python -m http.server "
            "--directory=./docs/_build "
            "--bind=127.0.0.1"
        )


@fabric.task  # type: ignore[misc]
def python(context, command):
    # type: (invoke.context.Context, str) -> None
    """Alias to 'python' inside container."""
    clink(
        context,
        command=command,
        options="--no-deps --entrypoint=/opt/venv-3.6/bin/python ",
    )


@fabric.task  # type: ignore[misc]
def run(context, service, command="", options="", uid=uid, gid=gid):
    # type: (invoke.context.Context, str, str, str, int, int) -> None
    """Alias to docker compose run."""
    with context.cd("."):
        invoke.run(
            f"docker compose run --rm --user={uid}:{gid} "
            f"{options} {service} {command}",
            pty=True,
        )


@fabric.task  # type: ignore[misc]
def shell(context):
    # type: (invoke.context.Context) -> None
    """Launch python shell in project container."""
    with context.cd("."):
        clink(
            context,
            options=(
                "--no-deps "
                f"--volume={project_name}_ipython:/home/user/.ipython "
                f"--entrypoint=chown"
            ),
            command=f"{uid}:{gid} /home/user/.ipython",
            uid=0,
            gid=0,
        )
    uv(
        context,
        options=f"--volume={project_name}_ipython:/home/user/.ipython",
        command=(
            "run "
            "--cache-dir=/var/cache/uv "
            "--project=/opt/venv-3.7 "
            "--python=/opt/venv-3.7/bin/python "
            "--with=ipython "
            "ipython -i"
        ),
    )


@fabric.task  # type: ignore[misc]
def test(context, scope=None):
    # type: (invoke.context.Context, typing.Optional[str]) -> None
    """Run tests."""
    envlist = "python3.6,coverage" if scope is None else "python3.6"
    tox(context, envlist=envlist, scope=scope)


@fabric.task  # type: ignore[misc]
def tox(context, envlist=None, scope=None):
    # type: (invoke.context.Context, typing.Optional[str], typing.Optional[str]) -> None  # noqa: E501
    """Run tox."""
    with context.cd("."):
        clink(
            context,
            command=f"{uid}:{gid} /opt/project/.tox",
            options=(
                "--no-deps "
                f"--volume={project_name}_tox:/opt/project/.tox "
                "--entrypoint=chown"
            ),
            uid=0,
            gid=0,
        )
    command = f"-e {envlist}" if envlist else "--parallel"
    if scope:
        command = f"{command} -- {scope}"
    uv(
        context,
        command=f"tool run --cache-dir=/var/cache/uv tox {command}",
        options=f"--volume={project_name}_tox:/opt/project/.tox",
    )


@fabric.task  # type: ignore[misc]
def uv(context, command, options=""):
    # type: (invoke.context.Context, str, str) -> None
    """Alias to 'uv' inside container."""
    with context.cd("."):
        clink(
            context,
            command=f"{uid}:{gid} /var/cache/uv",
            options=(
                "--no-deps "
                f"--volume={project_name}_uv:/var/cache/uv "
                "--entrypoint=chown"
            ),
            uid=0,
            gid=0,
        )

    clink(
        context,
        command=command,
        options=(
            "--entrypoint=uv "
            f"--volume={project_name}_uv:/var/cache/uv "
            f"{options}"
        ),
    )


@fabric.task  # type: ignore[misc]
def wheel(context):
    # type: (invoke.context.Context) -> None
    """Build project python wheel."""
    with context.cd("."):
        invoke.run("rm -rf ./build")
    uv(
        context,
        command=(
            "tool run --cache-dir=/var/cache/uv --from build "
            "pyproject-build --wheel"
        ),
        options='--no-deps -e PYTHONDONTWRITEBYTECODE="" ',
    )
    with context.cd("."):
        invoke.run("rm -rf ./build")


@fabric.task  # type: ignore[misc]
def clink(context, command="", options="", uid=uid, gid=gid):
    # type: (invoke.context.Context, str, str, int, int) -> None
    run(
        context,
        service="clink",
        command=command,
        options=options,
        uid=uid,
        gid=gid,
    )
