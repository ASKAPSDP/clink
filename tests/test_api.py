import importlib
import unittest
from unittest import mock

from clink import api, handlers, listeners, messaging


class ImportTestCase(unittest.TestCase):
    def test_can_import_api(self) -> None:
        importlib.import_module(".api", package="clink")

    def test_has_event(self) -> None:
        self.assertTrue(hasattr(api, "Event"))
        self.assertEqual(messaging.Event, api.Event)

    def test_has_traceparent(self) -> None:
        self.assertTrue(hasattr(api, "TraceParent"))
        self.assertEqual(messaging.trace_context.TraceParent, api.TraceParent)

    def test_has_tracestate(self) -> None:
        self.assertTrue(hasattr(api, "TraceState"))
        self.assertEqual(messaging.trace_context.TraceState, api.TraceState)


class ParticipantTestCase(unittest.TestCase):
    def test___init___calls_producer_with_dispatcher_and_name_as_source(
        self,
    ) -> None:
        dispatcher = mock.Mock()

        with mock.patch("clink.participants.Producer.__init__") as producer:
            api.Participant("TEST_NAME", dispatcher=dispatcher)

        producer.assert_called_once_with(dispatcher, source="TEST_NAME")

    def test_on_event_creates_event_handler_and_listener(self) -> None:
        dispatcher = mock.Mock()
        participant = api.Participant("TEST_NAME", dispatcher=dispatcher)
        callable = mock.Mock()

        wrapped = participant.on_event(
            "TEST_TOPIC_1", "TEST_TOPIC_2", supports_dry_run="TEST_DRY_RUN"
        )(callable)

        callable.assert_not_called()

        self.assertIsInstance(wrapped._handler, handlers.EventHandler)
        self.assertEqual("TEST_DRY_RUN", wrapped._handler.supports_dry_run)
        self.assertEqual(dispatcher, wrapped._handler.dispatcher)

        listener = participant.listeners[0]

        self.assertIsInstance(listener, listeners.Listener)
        self.assertEqual(["TEST_TOPIC_1", "TEST_TOPIC_2"], listener.topics)
        self.assertEqual(wrapped, listener.handlers[0])
