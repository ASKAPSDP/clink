import unittest
from unittest import mock

from clink import listeners


class ListenerTestCase(unittest.TestCase):
    def test___post_init___calls_maybe_list_with_topic(self) -> None:
        topics = mock.Mock()
        with mock.patch("clink.utils.maybe_list") as maybe_list:
            listeners.Listener(topics=topics, handlers=mock.Mock())

        maybe_list.assert_has_calls([mock.call(topics)])

    def test___post_init___sets_topic_to_list_when_provided_string(
        self,
    ) -> None:
        topics = "TEST_TOPIC"
        listener = listeners.Listener(topics=topics, handlers=mock.Mock())

        self.assertIsInstance(listener.topics, list)
        self.assertEqual(topics, listener.topics[0])  # type: ignore[index]

    def test___post_init___sets_topic_to_list_when_given_list(self) -> None:
        topics = ["TEST_TOPIC_1", "TEST_TOPIC_2"]
        listener = listeners.Listener(topics=topics, handlers=mock.Mock())

        self.assertIsInstance(listener.topics, list)
        self.assertEqual(topics, listener.topics)

    def test___post_init___calls_maybe_list_with_handlers(self) -> None:
        handlers = mock.Mock()
        with mock.patch("clink.utils.maybe_list") as maybe_list:
            listeners.Listener(topics=mock.Mock(), handlers=handlers)

        maybe_list.assert_has_calls([mock.call(handlers)])

    def test___post_init___sets_handlers_to_list_when_provided_callable(
        self,
    ) -> None:
        handlers = mock.Mock()
        listener = listeners.Listener(topics=mock.Mock(), handlers=handlers)

        self.assertIsInstance(listener.handlers, list)
        self.assertEqual(handlers, listener.handlers[0])  # type: ignore[index]

    def test___post_init___sets_handlers_to_list_when_given_list(self) -> None:
        handlers = [mock.Mock(), mock.Mock()]
        listener = listeners.Listener(topics=mock.Mock(), handlers=handlers)

        self.assertIsInstance(listener.handlers, list)
        self.assertEqual(handlers, listener.handlers)

    def test_durable_returns_false_when_name_is_none(self) -> None:
        listener = listeners.Listener(
            topics=mock.Mock(), handlers=mock.Mock(), name=None
        )

        self.assertFalse(listener.durable)

    def test_durable_returns_true_when_name_is_not_none(self) -> None:
        listener = listeners.Listener(
            topics=mock.Mock(), handlers=mock.Mock(), name="TEST_NAME"
        )

        self.assertTrue(listener.durable)

    def test_exclusive_returns_true_when_name_is_none(self) -> None:
        listener = listeners.Listener(
            topics=mock.Mock(), handlers=mock.Mock(), name=None
        )

        self.assertTrue(listener.exclusive)

    def test_exclusive_returns_false_when_name_is_not_none(self) -> None:
        listener = listeners.Listener(
            topics=mock.Mock(), handlers=mock.Mock(), name="TEST_NAME"
        )

        self.assertFalse(listener.exclusive)


class ParseListenerReferencesTestCase(unittest.TestCase):
    @mock.patch("clink.handlers._parse_handler_references")
    def test_returns_list_of_one_listener_given_one_listener(
        self, _parse_handler_references: mock.Mock
    ) -> None:
        value = listeners.Listener(topics=[], handlers=[])

        result = listeners._parse_listener_references(
            listeners=value, context=None
        )

        self.assertIsInstance(result, list)
        self.assertEqual(result, [value])
        _parse_handler_references.assert_not_called()

    @mock.patch("clink.handlers._parse_handler_references")
    def test_returns_list_of_one_listener_given_list_of_one_listener(
        self, _parse_handler_references: mock.Mock
    ) -> None:
        value = [listeners.Listener(topics=[], handlers=[])]

        result = listeners._parse_listener_references(
            listeners=value, context=None
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)
        self.assertEqual(result, value)
        _parse_handler_references.assert_not_called()

    @mock.patch("clink.handlers._parse_handler_references")
    def test_returns_list_of_two_listeners_given_list_of_two_listeners(
        self, _parse_handler_references: mock.Mock
    ) -> None:
        value = [
            listeners.Listener(topics=[], handlers=[]),
            listeners.Listener(topics=[], handlers=[]),
        ]

        result = listeners._parse_listener_references(
            listeners=value, context=None
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertEqual(result, value)
        _parse_handler_references.assert_not_called()

    def test_returns_list_of_one_listener_given_one_dict_key_and_string_value(
        self,
    ) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners={"topic": "handler"}, context=context
            )

        _parse_handler_references.assert_called_once_with(
            "handler", context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)

        item = result[0]

        self.assertIsInstance(item, listeners.Listener)
        self.assertEqual(item.topics, ["topic"])
        self.assertEqual(item.handlers, parsed_handlers)

    def test_returns_list_of_one_listener_given_one_dict_key_and_two_string_values(  # noqa: E501
        self,
    ) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock(), mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners={"topic": ["handler_one", "handler_two"]},
                context=context,
            )

        _parse_handler_references.assert_called_once_with(
            ["handler_one", "handler_two"], context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)

        item = result[0]

        self.assertIsInstance(item, listeners.Listener)
        self.assertEqual(item.topics, ["topic"])
        self.assertEqual(item.handlers, parsed_handlers)

    def test_returns_list_of_two_listeners_given_two_dict_keys_and_string_values(  # noqa: E501
        self,
    ) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners={
                    "topic_one": "handler_one",
                    "topic_two": "handler_two",
                },
                context=context,
            )

        _parse_handler_references.assert_has_calls(
            [
                mock.call("handler_one", context=context),
                mock.call("handler_two", context=context),
            ]
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)

        self.assertIsInstance(result[0], listeners.Listener)
        self.assertEqual(result[0].topics, ["topic_one"])
        self.assertEqual(result[0].handlers, parsed_handlers)

        self.assertIsInstance(result[1], listeners.Listener)
        self.assertEqual(result[1].topics, ["topic_two"])
        self.assertEqual(result[1].handlers, parsed_handlers)

    def test_returns_list_of_two_listeners_given_two_dict_keys_each_with_two_string_values(  # noqa: E501
        self,
    ) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners={
                    "topic_one": ["handler_one", "handler_two"],
                    "topic_two": ["handler_three", "handler_four"],
                },
                context=context,
            )

        _parse_handler_references.assert_has_calls(
            [
                mock.call(["handler_one", "handler_two"], context=context),
                mock.call(["handler_three", "handler_four"], context=context),
            ]
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)

        self.assertIsInstance(result[0], listeners.Listener)
        self.assertEqual(result[0].topics, ["topic_one"])
        self.assertEqual(result[0].handlers, parsed_handlers)

        self.assertIsInstance(result[1], listeners.Listener)
        self.assertEqual(result[1].topics, ["topic_two"])
        self.assertEqual(result[1].handlers, parsed_handlers)

    def test_returns_list_of_one_listener_given_one_dict(self) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners={"topics": "topic", "handlers": "handler"},
                context=context,
            )

        _parse_handler_references.assert_called_once_with(
            "handler", context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)

        item = result[0]

        self.assertIsInstance(item, listeners.Listener)
        self.assertEqual(item.topics, ["topic"])
        self.assertEqual(item.handlers, parsed_handlers)

    def test_returns_list_of_one_listener_given_list_of_one_dict(self) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners=[{"topics": "topic", "handlers": "handler"}],
                context=context,
            )

        _parse_handler_references.assert_called_once_with(
            "handler", context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)

        item = result[0]

        self.assertIsInstance(item, listeners.Listener)
        self.assertEqual(item.topics, ["topic"])
        self.assertEqual(item.handlers, parsed_handlers)

    def test_returns_list_of_two_listeners_given_list_of_two_dicts(
        self,
    ) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners=[
                    {"topics": "topic_one", "handlers": "handler_one"},
                    {"topics": "topic_two", "handlers": "handler_two"},
                ],
                context=context,
            )

        _parse_handler_references.assert_has_calls(
            [
                mock.call("handler_one", context=context),
                mock.call("handler_two", context=context),
            ]
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)

        self.assertIsInstance(result[0], listeners.Listener)
        self.assertEqual(result[0].topics, ["topic_one"])
        self.assertEqual(result[0].handlers, parsed_handlers)

        self.assertIsInstance(result[1], listeners.Listener)
        self.assertEqual(result[1].topics, ["topic_two"])
        self.assertEqual(result[1].handlers, parsed_handlers)

    def test_returns_list_of_two_listeners_given_list_of_two_dicts_with_lists(
        self,
    ) -> None:
        context = mock.Mock()
        parsed_handlers = [mock.Mock()]

        with mock.patch(
            "clink.handlers._parse_handler_references",
            return_value=parsed_handlers,
        ) as _parse_handler_references:
            result = listeners._parse_listener_references(
                listeners=[
                    {
                        "topics": ["topic_one", "topic_two"],
                        "handlers": ["handler_one", "handler_two"],
                    },
                    {
                        "topics": ["topic_three", "topic_four"],
                        "handlers": ["handler_three", "handler_four"],
                    },
                ],
                context=context,
            )

        _parse_handler_references.assert_has_calls(
            [
                mock.call(["handler_one", "handler_two"], context=context),
                mock.call(["handler_three", "handler_four"], context=context),
            ]
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)

        self.assertIsInstance(result[0], listeners.Listener)
        self.assertEqual(result[0].topics, ["topic_one", "topic_two"])
        self.assertEqual(result[0].handlers, parsed_handlers)

        self.assertIsInstance(result[1], listeners.Listener)
        self.assertEqual(result[1].topics, ["topic_three", "topic_four"])
        self.assertEqual(result[1].handlers, parsed_handlers)


class ListenerListTestCase(unittest.TestCase):
    def test___add__calls__parse_listener_references_on_other(self) -> None:
        test_value = mock.Mock()
        instance = listeners.ListenerList()

        with mock.patch(
            "clink.listeners._parse_listener_references"
        ) as _parse_listener_references:
            instance.__add__(test_value)

        self.assertEqual(
            mock.call(test_value, context=None),
            _parse_listener_references.call_args_list[0],
        )

    def test___add__calls__parse_listener_references_on_other_with_context(
        self,
    ) -> None:
        test_value = mock.Mock()
        context = mock.Mock()
        instance = listeners.ListenerList(context=context)

        with mock.patch(
            "clink.listeners._parse_listener_references"
        ) as _parse_listener_references:
            instance.__add__(test_value)

        self.assertEqual(
            mock.call(test_value, context=context),
            _parse_listener_references.call_args_list[0],
        )

    def test___iadd__calls__parse_listener_references_on_other(self) -> None:
        test_value = mock.Mock()
        instance = listeners.ListenerList()

        with mock.patch(
            "clink.listeners._parse_listener_references"
        ) as _parse_listener_references:
            instance.__iadd__(test_value)

        _parse_listener_references.assert_called_once_with(
            test_value, context=None
        )

    def test___iadd__calls__parse_listener_references_on_other_with_context(
        self,
    ) -> None:
        test_value = mock.Mock()
        context = mock.Mock()
        instance = listeners.ListenerList(context=context)

        with mock.patch(
            "clink.listeners._parse_listener_references"
        ) as _parse_listener_references:
            instance.__iadd__(test_value)

        _parse_listener_references.assert_called_once_with(
            test_value, context=context
        )

    def test___init__calls__parse_listener_references_on_initlist(
        self,
    ) -> None:
        test_list = mock.Mock()

        with mock.patch(
            "clink.listeners._parse_listener_references"
        ) as _parse_listener_references:
            listeners.ListenerList(test_list)

        _parse_listener_references.assert_called_once_with(
            test_list, context=None
        )

    def test___init__calls__parse_listener_references_on_initlist_with_context(
        self,
    ) -> None:
        test_list = mock.Mock()
        context = mock.Mock()

        with mock.patch(
            "clink.listeners._parse_listener_references"
        ) as _parse_listener_references:
            listeners.ListenerList(test_list, context=context)

        _parse_listener_references.assert_called_once_with(
            test_list, context=context
        )

    def test_add_with_another_listener_list(self) -> None:
        one = listeners.Listener(topics=["one"], handlers=[mock.Mock()])
        two = listeners.Listener(topics=["two"], handlers=[mock.Mock()])

        list_one = listeners.ListenerList([one])
        list_two = listeners.ListenerList([two])

        result = list_one + list_two

        self.assertEqual([one, two], result)

    def test_iadd_with_another_listener_list(self) -> None:
        one = listeners.Listener(topics=["one"], handlers=[mock.Mock()])
        two = listeners.Listener(topics=["two"], handlers=[mock.Mock()])

        result = listeners.ListenerList([one])
        result += listeners.ListenerList([two])

        self.assertEqual([one, two], result)
