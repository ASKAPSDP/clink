import importlib
import unittest


class ImportTestCase(unittest.TestCase):
    def test_can_import_module(self) -> None:
        importlib.import_module(".clink", package="clink")
