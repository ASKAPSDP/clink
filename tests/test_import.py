import importlib
import unittest


class ImportTestCase(unittest.TestCase):
    def test_can_import_module(self) -> None:
        importlib.import_module(".", package="clink")

    def test_can_import___main__(self) -> None:
        importlib.import_module(".__main__", package="clink")
