import datetime
import unittest
from typing import Any, Dict
from unittest import mock

import click
import click.testing
import yaml

from clink import __main__ as cli
from clink import api as clink


try:
    import zoneinfo
except ImportError:  # Python < 3.9
    from backports import zoneinfo


@mock.patch("logging.basicConfig")
@mock.patch("logging.getLogger")
class MainTestCase(unittest.TestCase):
    @mock.patch("rich.console.Console")
    def test_sets_component_on_context_obj(self, *args: mock.Mock) -> None:
        obj: Dict[str, Any] = {"test": "value"}

        with click.Context(click.Command(name="main"), obj=obj):
            cli.main.callback()  # type: ignore[misc]

        self.assertIn("component", obj)
        self.assertIsInstance(obj["component"], clink.Participant)
        self.assertEqual("clink.cli", obj["component"].source)

    @mock.patch("rich.console.Console")
    def test_calls_console_print_with_expected_kwargs(
        self, *args: mock.Mock
    ) -> None:
        context: Dict[str, Any] = {}

        with click.Context(click.Command(name="main"), obj=context):
            cli.main.callback()  # type: ignore[misc]

        console = context["stderr_console"]
        console.print.assert_called_once()

        args, kwargs = console.print.call_args

        self.assertIsInstance(args[0], str)
        self.assertEqual(
            kwargs,
            {
                "crop": True,
                "highlight": False,
                "no_wrap": True,
                "overflow": "crop",
                "style": "bold #c837ab",
                "width": 80,
            },
        )

    @mock.patch(
        "rich.console.Console",
        return_value=unittest.mock.Mock(is_terminal=False),
    )
    def test_does_not_call_console_print_when_console_is_not_terminal(
        self, *args: mock.Mock
    ) -> None:
        context: Dict[str, Any] = {}

        with click.Context(click.Command(name="main"), obj=context):
            cli.main.callback()  # type: ignore[misc]

        console = context["stderr_console"]
        console.print.assert_not_called()


@mock.patch("clink.participants.Producer.emit_event")
class EmitEventTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.component = clink.Participant("clink.tests")
        self.runner = click.testing.CliRunner()
        self.context: Dict[str, Any] = {"obj": {"component": self.component}}

    def test_succeeds(self, *args: mock.Mock) -> None:
        result = self.runner.invoke(
            cli.emit_event, ["TEST_TOPIC"], **self.context
        )

        self.assertEqual(0, result.exit_code)

    def test_emits_event_with_provided_type(
        self, emit_event: mock.Mock
    ) -> None:
        result = self.runner.invoke(
            cli.emit_event, ["TEST_TYPE"], **self.context
        )

        self.assertEqual(0, result.exit_code)

        emit_event.assert_called_once_with(
            type="TEST_TYPE", subject=None, dry_run=False
        )

    def test_emits_event_with_provided_subject(
        self, emit_event: mock.Mock
    ) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["TEST_TYPE", "--subject=TEST_SUBJECT"],
            **self.context,
        )

        self.assertEqual(0, result.exit_code)

        emit_event.assert_called_once_with(
            type="TEST_TYPE", subject="TEST_SUBJECT", dry_run=False
        )

    def test_emits_event_with_dry_run(self, emit_event: mock.Mock) -> None:
        result = self.runner.invoke(
            cli.emit_event, ["TEST_TYPE", "--dry-run"], **self.context
        )

        self.assertEqual(0, result.exit_code)

        emit_event.assert_called_once_with(
            type="TEST_TYPE", subject=None, dry_run=True
        )

    def test_emits_event_from_json_file(self, emit_event: mock.Mock) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["--input=./tests/fixtures/event.json"],
            **self.context,
        )

        self.assertEqual(0, result.exit_code)

        args, _ = emit_event.call_args
        event = args[0]

        self.assertIsInstance(event, clink.Event)

        self.assertTrue(event.dry_run)
        self.assertEqual(str(event.id), "b6ce809c-bc36-11eb-bcc0-0242ac150005")
        self.assertEqual(event.source, "clink.tests.cli")
        self.assertEqual(event.spec_version, "1.0")
        self.assertEqual(
            event.time,
            datetime.datetime(
                2021, 5, 24, 2, 21, 14, 123456, tzinfo=zoneinfo.ZoneInfo("UTC")
            ),
        )
        self.assertEqual(event.trace_parent.parent_id, "8f9dc852f924b3bc")
        self.assertEqual(
            event.trace_parent.trace_id, "480b74cc4caf40cb88676157f2cf0b52"
        )
        self.assertEqual(
            event.trace_state["clink-tests-cli"], "8f9dc852f924b3bc"
        )
        self.assertEqual(event.type, "event.test")

    def test_emits_event_from_yaml_file(self, emit_event: mock.Mock) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["--input=./tests/fixtures/event.yaml"],
            **self.context,
        )

        self.assertEqual(0, result.exit_code)

        args, kwargs = emit_event.call_args
        event = args[0]

        self.assertIsInstance(event, clink.Event)

        self.assertFalse(event.dry_run)
        self.assertEqual(str(event.id), "dbdf672a-bc36-11eb-bcc0-0242ac150005")
        self.assertEqual(event.source, "clink.tests.cli")
        self.assertEqual(event.spec_version, "1.0")
        self.assertEqual(
            event.time,
            datetime.datetime(
                2021, 5, 24, 2, 22, 16, 804129, tzinfo=zoneinfo.ZoneInfo("UTC")
            ),
        )
        self.assertEqual(event.trace_parent.parent_id, "a3bb0449d1d6f69d")
        self.assertEqual(
            event.trace_parent.trace_id, "3cc2acf4ce5d490b833845ed78799b49"
        )
        self.assertEqual(
            event.trace_state["clink-tests-cli"], "a3bb0449d1d6f69d"
        )
        self.assertEqual(event.type, "event.test")

        self.assertFalse(kwargs["dry_run"])

    def test_emits_event_from_yaml_file_and_dry_run_flag(
        self, emit_event: mock.Mock
    ) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["--input=./tests/fixtures/event.yaml", "--dry-run"],
            **self.context,
        )

        self.assertEqual(0, result.exit_code)

        args, kwargs = emit_event.call_args
        event = args[0]

        self.assertFalse(event.dry_run)

        self.assertTrue(kwargs["dry_run"])

    def test_errors_when_neither_type_nor_input_are_provided(
        self, *args: mock.Mock
    ) -> None:
        result = self.runner.invoke(cli.emit_event, [], **self.context)

        self.assertNotEqual(0, result.exit_code)
        self.assertIn(
            'Either "type" OR "--input" parameter must be provided',
            result.output,
        )

    def test_errors_when_both_type_and_input_are_provided(
        self, *args: mock.Mock
    ) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["TEST_TOPIC", "--input=./tests/fixtures/event.json"],
            **self.context,
        )

        self.assertNotEqual(0, result.exit_code)
        self.assertIn(
            'Either "type" OR "--input" parameter must be provided',
            result.output,
        )

    def test_errors_when_provided_invalid_file_path(
        self, *args: mock.Mock
    ) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["--input=./tests/fixtures/missing_test.json"],
            **self.context,
        )

        self.assertNotEqual(0, result.exit_code)
        self.assertIn("./tests/fixtures/missing_test.json", result.output)
        self.assertIn("No such file or directory", result.output)

    def test_errors_when_provided_invalid_yaml_file(
        self, *args: mock.Mock
    ) -> None:
        result = self.runner.invoke(
            cli.emit_event,
            ["--input=./tests/fixtures/invalid.yaml"],
            **self.context,
        )

        self.assertNotEqual(0, result.exit_code)
        self.assertIsInstance(result.exception, yaml.YAMLError)


@mock.patch("clink.participants.Consumer.consume")
class ConsumeTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.component = clink.Participant("clink.tests")
        self.runner = click.testing.CliRunner()
        self.context: Dict[str, Any] = {"obj": {"component": self.component}}

    def test_succeeds(self, *args: mock.Mock) -> None:
        result = self.runner.invoke(cli.consume, **self.context)

        self.assertEqual(0, result.exit_code)

    def test_calls_consume(self, consume: mock.Mock) -> None:
        self.runner.invoke(cli.consume, **self.context)

        consume.assert_called_once_with()

    def test_sets_default_topic(self, *args: mock.Mock) -> None:
        self.runner.invoke(cli.consume, **self.context)

        self.assertEqual(["#"], self.component.listeners[0].topics)

    def test_sets_provided_topic(self, *args: mock.Mock) -> None:
        self.runner.invoke(cli.consume, ["--topic=TEST_TOPIC"], **self.context)

        self.assertEqual(["TEST_TOPIC"], self.component.listeners[0].topics)

    def test_sets_provided_topics(self, *args: mock.Mock) -> None:
        self.runner.invoke(
            cli.consume,
            ["--topic=TEST_TOPIC_ONE", "--topic=TEST_TOPIC_TWO"],
            **self.context,
        )

        self.assertEqual(
            ["TEST_TOPIC_ONE", "TEST_TOPIC_TWO"],
            self.component.listeners[0].topics,
        )
