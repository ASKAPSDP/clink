import logging
import unittest
from unittest import mock

from clink import listeners, messaging, participants


try:
    from typing_extensions import TypeAlias
except ImportError:
    from typing import TypeAlias  # type: ignore[attr-defined, no-redef]


try:
    AsyncioTestCase: TypeAlias = unittest.IsolatedAsyncioTestCase
except AttributeError:
    import asynctest

    AsyncioTestCase = asynctest.TestCase  # type: ignore[misc]


class ParticipantTestCase(unittest.TestCase):
    def test___init___sets_dispatcher_to_provided_argument(self) -> None:
        dispatcher = mock.Mock()

        instance = participants.Participant(dispatcher=dispatcher)

        self.assertEqual(instance.dispatcher, dispatcher)

    def test___init___sets_dispatcher_to_default_if_not_provided(self) -> None:
        dispatcher = mock.Mock()

        with mock.patch("clink.backends.Dispatcher", return_value=dispatcher):
            instance = participants.Participant()

        self.assertEqual(instance.dispatcher, dispatcher)


class ConsumerTestCase(unittest.TestCase):
    def test___init___sets_listeners_to_listenerlist_with_self_listeners(
        self,
    ) -> None:
        mock_listeners = mock.Mock()

        class TestConsumer(participants.Consumer):
            listeners = mock_listeners

        with mock.patch(
            "clink.listeners.ListenerList.__init__", return_value=None
        ) as listenerlist:
            instance = TestConsumer()

        listenerlist.assert_called_once_with(mock_listeners, context=instance)

    def test___init___adds_listeners_to_listenerlist(self) -> None:
        mock_listeners = mock.Mock()

        with mock.patch("clink.listeners.ListenerList.__iadd__") as iadd:
            participants.Consumer(listeners=mock_listeners)

        iadd.assert_called_once_with(mock_listeners)

    def test___init___combines_self_listeners_and_arg_listeners(self) -> None:
        listener_one = listeners.Listener(
            topics=["one"], handlers=[mock.Mock()]
        )
        listener_two = listeners.Listener(
            topics=["two"], handlers=[mock.Mock()]
        )

        class TestConsumer(participants.Consumer):
            listeners = [listener_one]

        instance = TestConsumer(listeners=[listener_two])

        self.assertIn(listener_one, instance.listeners)
        self.assertIn(listener_two, instance.listeners)
        self.assertEqual([listener_one, listener_two], instance.listeners)

    def test_consume_calls_dispatcher_consume(self) -> None:
        dispatcher = mock.Mock()
        consumer = participants.Consumer(dispatcher=dispatcher)

        consumer.consume()

        dispatcher.consume.assert_called_once_with(consumer=consumer)


class AsyncConsumerTestCase(AsyncioTestCase):
    async def test_async_consume_calls_run_in_executor_with_consume(
        self,
    ) -> None:
        with mock.patch("clink.participants.Consumer.consume") as consume:
            consumer = participants.Consumer(dispatcher=mock.Mock())
            await consumer.async_consume()

        consume.assert_called_once_with()


class ProducerTestCase(unittest.TestCase):
    def test___init___does_not_set_source(self) -> None:
        producer = participants.Producer()

        self.assertIsNone(producer.source)

    def test___init___does_not_set_source_when_none(self) -> None:
        class TestProducer(participants.Producer):
            source = "TEST_SOURCE"

        producer = TestProducer(source=None)

        self.assertEqual(producer.source, "TEST_SOURCE")

    def test___init___sets_source_when_not_none(self) -> None:
        producer = participants.Producer(source="TEST_SOURCE")

        self.assertEqual(producer.source, "TEST_SOURCE")

    def test_emit_event_calls_dispatcher_emit_event(self) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        event = messaging.Event(source="TEST_SOURCE", type="TEST_TYPE")

        producer.emit_event(event=event)

        dispatcher.emit_event.assert_called_once()

        _, kwargs = dispatcher.emit_event.call_args

        self.assertEqual(producer, kwargs["producer"])
        self.assertEqual(event, kwargs["event"])

    def test_emit_event_calls_dispatcher_emit_event_with_event_dry_run_true_if_provided_explicitly_and_true(  # noqa: E501
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        event = messaging.Event(source="TEST_SOURCE", type="TEST_TYPE")

        producer.emit_event(event=event, dry_run=True)

        dispatcher.emit_event.assert_called_once()

        _, kwargs = dispatcher.emit_event.call_args

        self.assertEqual(producer, kwargs["producer"])
        self.assertFalse(event.dry_run)
        self.assertTrue(kwargs["event"].dry_run)

    def test_emit_event_calls_dispatcher_emit_event_with_event_dry_run_false_if_provided_explicitly_and_false(  # noqa: E501
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        event = messaging.Event(source="TEST_SOURCE", type="TEST_TYPE")

        producer.emit_event(event=event, dry_run=False)

        dispatcher.emit_event.assert_called_once()

        _, kwargs = dispatcher.emit_event.call_args

        self.assertEqual(producer, kwargs["producer"])
        self.assertFalse(event.dry_run)
        self.assertFalse(kwargs["event"].dry_run)

    def test_emit_event_with_dry_run_true_calls_dispatcher_emit_event_with_dry_run_true_if_provided_explicitly_and_false(  # noqa: E501
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        event = messaging.Event(
            source="TEST_SOURCE", type="TEST_TYPE", dry_run=True
        )

        producer.emit_event(event=event, dry_run=False)

        dispatcher.emit_event.assert_called_once()

        _, kwargs = dispatcher.emit_event.call_args

        self.assertEqual(producer, kwargs["producer"])
        self.assertTrue(event.dry_run)
        self.assertTrue(kwargs["event"].dry_run)

    def test_emit_event_with_dry_run_false_calls_dispatcher_emit_event_with_dry_run_true_if_provided_explicitly_and_false(  # noqa: E501
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        event = messaging.Event(
            source="TEST_SOURCE", type="TEST_TYPE", dry_run=False
        )

        producer.emit_event(event=event, dry_run=False)

        dispatcher.emit_event.assert_called_once()

        _, kwargs = dispatcher.emit_event.call_args

        self.assertEqual(producer, kwargs["producer"])
        self.assertFalse(event.dry_run)
        self.assertFalse(kwargs["event"].dry_run)

    def test_emit_event_logs_info(self) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        event = messaging.Event(
            type="TEST_TYPE", subject="TEST_SUBJECT", source="TEST_SOURCE"
        )

        with self.assertLogs(level=logging.INFO) as context:
            producer.emit_event(event=event)

        self.assertIn(
            'Published "TEST_TYPE" event from "TEST_SOURCE" '
            'for "TEST_SUBJECT"',
            context.output[-1],
        )
        self.assertIn(str(event.id), context.output[-1])
        self.assertIn(event.trace_parent.trace_id, context.output[-1])
        self.assertIn(event.trace_parent.parent_id, context.output[-1])

    def test_emit_event_constructs_event_from_kwargs_and_default_source(
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(
            dispatcher=dispatcher, source="TEST_SOURCE"
        )

        producer.emit_event(type="TEST_TYPE", subject="TEST_SUBJECT")

        _, kwargs = dispatcher.emit_event.call_args
        event = kwargs["event"]

        self.assertIsInstance(event, messaging.Event)
        self.assertEqual("TEST_TYPE", event.type)
        self.assertEqual("TEST_SUBJECT", event.subject)
        self.assertEqual("TEST_SOURCE", event.source)

    def test_emit_event_constructs_event_from_kwargs_and_specified_source(
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(
            dispatcher=dispatcher, source="TEST_SOURCE"
        )

        producer.emit_event(
            source="TEST_ANOTHER_SOURCE",
            type="TEST_TYPE",
            subject="TEST_SUBJECT",
        )

        _, kwargs = dispatcher.emit_event.call_args
        event = kwargs["event"]

        self.assertIsInstance(event, messaging.Event)
        self.assertEqual("TEST_TYPE", event.type)
        self.assertEqual("TEST_SUBJECT", event.subject)
        self.assertEqual("TEST_ANOTHER_SOURCE", event.source)

    def test_emit_event_constructs_event_from_preceding_event_when_provided(
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(
            dispatcher=dispatcher, source="TEST_SOURCE"
        )

        preceding_event = messaging.Event(
            source="TEST_SOURCE", type="TEST_ONE"
        )

        with mock.patch(
            "clink.messaging.events.BaseEvent.from_preceding_event"
        ) as from_preceding_event:
            producer.emit_event(
                type="TEST_TWO", preceding_event=preceding_event
            )

        from_preceding_event.assert_called_once_with(
            preceding_event, source="TEST_SOURCE", type="TEST_TWO"
        )

    def test_emit_event_does_not_construct_event_from_preceding_event_when_not_provided(  # noqa: E501
        self,
    ) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(
            dispatcher=dispatcher, source="TEST_SOURCE"
        )

        with mock.patch(
            "clink.messaging.events.BaseEvent.from_preceding_event"
        ) as from_preceding_event:
            producer.emit_event(type="TEST_TWO")

        from_preceding_event.assert_not_called()

    def test_send_message_calls_dispatcher_send_message(self) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        message = mock.Mock()

        producer.send_message(message=message)

        dispatcher.send_message.assert_called_once_with(
            producer=producer, message=message
        )

    def test_send_message_constructs_message_from_kwargs(self) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(
            dispatcher=dispatcher, source="TEST_SOURCE"
        )

        producer.send_message()  # TODO

        _, kwargs = dispatcher.send_message.call_args
        message = kwargs["message"]

        self.assertIsInstance(message, messaging.base.BaseMessage)

    def test_run_command_calls_dispatcher_run_command(self) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(dispatcher=dispatcher)
        command = mock.Mock()

        producer.run_command(command=command)

        dispatcher.run_command.assert_called_once_with(
            producer=producer, command=command
        )

    def test_run_command_constructs_message_from_kwargs(self) -> None:
        dispatcher = mock.Mock()
        producer = participants.Producer(
            dispatcher=dispatcher, source="TEST_SOURCE"
        )

        producer.run_command()  # TODO

        _, kwargs = dispatcher.run_command.call_args
        command = kwargs["command"]

        self.assertIsInstance(command, messaging.commands.Command)


class AsyncProducerTestCase(AsyncioTestCase):
    async def test_async_emit_event_calls_run_in_executor_with_emit_event(
        self,
    ) -> None:
        event = mock.Mock()
        preceding_event = mock.Mock()

        with mock.patch(
            "clink.participants.Producer.emit_event"
        ) as emit_event:
            producer = participants.Producer(dispatcher=mock.Mock())
            await producer.async_emit_event(
                event, preceding_event=preceding_event
            )

        emit_event.assert_called_once_with(
            event, preceding_event=preceding_event
        )

    async def test_async_run_command_calls_run_in_executor_with_run_command(
        self,
    ) -> None:
        command = mock.Mock()

        with mock.patch(
            "clink.participants.Producer.run_command"
        ) as run_command:
            producer = participants.Producer(dispatcher=mock.Mock())
            await producer.async_run_command(command)

        run_command.assert_called_once_with(command)

    async def test_async_send_message_calls_run_in_executor_with_send_message(
        self,
    ) -> None:
        message = mock.Mock()

        with mock.patch(
            "clink.participants.Producer.send_message"
        ) as send_message:
            producer = participants.Producer(dispatcher=mock.Mock())
            await producer.async_send_message(message)

        send_message.assert_called_once_with(message)
