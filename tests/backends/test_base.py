import logging
import unittest
from typing import Any, Dict
from unittest import mock

from clink import messaging
from clink.backends import base


class TestDispatcher(base.Dispatcher):

    backend_class = base.Backend

    def consume(self, **kwargs: Any) -> None:
        raise NotImplementedError()

    def emit_event(self, **kwargs: Any) -> None:
        raise NotImplementedError()

    def get_metadata(self, **kwargs: Any) -> Dict[str, Any]:
        raise NotImplementedError()

    def run_command(self, **kwargs: Any) -> None:
        raise NotImplementedError()

    def send_message(self, **kwargs: Any) -> None:
        raise NotImplementedError()

    def to_event(self, **kwargs: Any) -> messaging.events.BaseEvent:
        raise NotImplementedError()


class DispatcherTestCase(unittest.TestCase):
    def test___init___sets_backend_to_instance_of_backend_class(self) -> None:
        dispatcher = TestDispatcher()
        self.assertIsInstance(dispatcher.backend, dispatcher.backend_class)

    def test___init___provides_self_to_backend(self) -> None:
        with mock.patch(
            "clink.backends.base.Backend.__init__", return_value=None
        ) as __init__:
            dispatcher = TestDispatcher()

        __init__.assert_called_once_with(dispatcher)

    @mock.patch("clink.__title__", "TEST_TITLE")
    @mock.patch("clink.__version__", "TEST_VERSION")
    @mock.patch("clink.__copyright__", "TEST_COPYRIGHT")
    def test___init___logs_title_and_version(self, *args: mock.Mock) -> None:
        with self.assertLogs(level=logging.INFO) as context:
            TestDispatcher()

        self.assertIn("TEST_TITLE", context.output[-1])
        self.assertIn("version TEST_VERSION", context.output[-1])
        self.assertIn("TEST_COPYRIGHT", context.output[-1])
