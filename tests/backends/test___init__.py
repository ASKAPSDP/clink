import unittest


class BackendsTestCase(unittest.TestCase):
    def test_backend_module_is_dummy_module(self) -> None:
        from clink import backends
        from clink.backends import dummy

        self.assertEqual(backends.backend_module, dummy)

    def test_dispatcher_is_dummy_dispatcher(self) -> None:
        from clink import backends
        from clink.backends import dummy

        self.assertEqual(backends.Dispatcher, dummy.Dispatcher)
