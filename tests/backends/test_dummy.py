import unittest
from unittest import mock

from clink.backends import dummy


class BackendTestCase(unittest.TestCase):
    def test___init___(self) -> None:
        dummy.Backend(dispatcher=mock.Mock())


class DispatcherTestCase(unittest.TestCase):
    def test_consume(self) -> None:
        instance = dummy.Dispatcher()

        instance.consume(consumer=mock.Mock())

    def test_emit_event(self) -> None:
        instance = dummy.Dispatcher()

        instance.emit_event(producer=mock.Mock(), event=mock.Mock())

    def test_get_metadata(self) -> None:
        instance = dummy.Dispatcher()

        instance.get_metadata()

    def test_run_command(self) -> None:
        instance = dummy.Dispatcher()

        instance.run_command(producer=mock.Mock(), command=mock.Mock())

    def test_send_message(self) -> None:
        instance = dummy.Dispatcher()

        instance.send_message(producer=mock.Mock(), message=mock.Mock())
