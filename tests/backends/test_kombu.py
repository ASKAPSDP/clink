import logging
import unittest
from unittest import mock

from clink.backends import kombu


class BackendTestCase(unittest.TestCase):
    def test___init___calls_exchange(self) -> None:
        with mock.patch(
            "kombu.Exchange.__init__", return_value=None
        ) as exchange:
            kombu.Backend(dispatcher=mock.Mock())

        exchange.assert_called_once_with(name="clink", type="topic")

    def test___init___sets_exchange_instance(self) -> None:
        exchange = mock.Mock()
        with mock.patch("kombu.Exchange", return_value=exchange):
            instance = kombu.Backend(dispatcher=mock.Mock())

        self.assertEqual(exchange, instance.exchange)

    def test_publish_sets__producer_when_not_already_set(self) -> None:
        instance = kombu.Backend(dispatcher=mock.Mock())
        producer = mock.Mock()

        self.assertFalse(hasattr(instance, "_producer"))

        with mock.patch(
            "clink.backends.kombu.Backend.get_consumerproducer",
            return_value=mock.Mock(producer=producer),
        ) as get_consumerproducer:
            instance.publish(
                body="TEST_BODY",
                content_type="TEST_CONTENT_TYPE",
                routing_key="TEST_ROUTING_KEY",
            )

        get_consumerproducer.assert_called_once_with()
        self.assertTrue(hasattr(instance, "_producer"))
        self.assertEqual(producer, instance._producer)

    def test_publish_does_not_set__producer_when_already_set(self) -> None:
        instance = kombu.Backend(dispatcher=mock.Mock())
        producer = mock.Mock()
        instance._producer = producer

        self.assertTrue(hasattr(instance, "_producer"))
        self.assertEqual(producer, instance._producer)

        with mock.patch(
            "clink.backends.kombu.Backend.get_consumerproducer",
        ) as get_consumerproducer:
            instance.publish(
                body="TEST_BODY",
                content_type="TEST_CONTENT_TYPE",
                routing_key="TEST_ROUTING_KEY",
            )

        get_consumerproducer.assert_not_called()
        self.assertEqual(producer, instance._producer)

    def test_publish_calls_kombu_producer_publish(self) -> None:
        instance = kombu.Backend(dispatcher=mock.Mock())
        producer = mock.Mock()

        with mock.patch(
            "clink.backends.kombu.Backend.get_consumerproducer",
            return_value=mock.Mock(producer=producer),
        ):
            instance.publish(
                body="TEST_BODY",
                content_type="TEST_CONTENT_TYPE",
                routing_key="TEST_ROUTING_KEY",
            )

        producer.publish.assert_called_once_with(
            body="TEST_BODY",
            content_type="TEST_CONTENT_TYPE",
            declare=[instance.exchange],
            exchange=instance.exchange,
            routing_key="TEST_ROUTING_KEY",
        )


class DispatcherTestCase(unittest.TestCase):
    def test__get_kombu_consumer_kwargs_sets_queue_auto_delete_if_not_durable(
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(  # note: exclusive implies auto-delete
            durable=False, exclusive=False, handlers=[], topics=[]
        )

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertTrue(queue.auto_delete)

    def test__get_kombu_consumer_kwargs_does_not_set_queue_auto_delete_if_durable(  # noqa: E501
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(  # note: exclusive implies auto-delete
            durable=True, exclusive=False, handlers=[], topics=[]
        )
        listener.name = None

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertFalse(queue.auto_delete)

    def test__get_kombu_consumer_kwargs_sets_queue_durable_if_durable(
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(durable=True, handlers=[], topics=[])

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertTrue(queue.durable)

    def test__get_kombu_consumer_kwargs_does_not_set_queue_durable_if_not_durable(  # noqa: E501
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(durable=False, handlers=[], topics=[])

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertFalse(queue.durable)

    def test__get_kombu_consumer_kwargs_sets_queue_exclusive_if_exclusive(
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(exclusive=True, handlers=[], topics=[])

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertTrue(queue.exclusive)

    def test__get_kombu_consumer_kwargs_does_not_set_queue_exclusive_if_not_exclusive(  # noqa: E501
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(exclusive=False, handlers=[], topics=[])

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertFalse(queue.exclusive)

    def test__get_kombu_consumer_kwargs_sets_queue_name(self) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(handlers=[], topics=[])
        listener.name = "TEST_NAME"

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]

        self.assertEqual("TEST_NAME", queue.name)

    def test__get_kombu_consumer_kwargs_sets_queue_bindings(self) -> None:
        dispatcher = kombu.Dispatcher()
        listener = mock.Mock(
            handlers=[], topics=["TEST_TOPIC_ONE", "TEST_TOPIC_TWO"]
        )

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[listener]
        )

        queue = result[0]["queues"][0]
        bindings = list(queue.bindings)

        self.assertEqual(2, len(bindings))

        routing_keys = [binding.routing_key for binding in bindings]

        self.assertIn("TEST_TOPIC_ONE", routing_keys)
        self.assertIn("TEST_TOPIC_TWO", routing_keys)

    def test__get_kombu_consumer_kwargs_wraps_and_sets_callbacks(self) -> None:
        dispatcher = kombu.Dispatcher()
        handler = mock.Mock()
        wrapped = mock.Mock()
        listener = mock.Mock(handlers=[handler], topics=[])

        with mock.patch(
            "clink.backends.kombu.Dispatcher._wrap_handler",
            return_value=wrapped,
        ) as _wrap_handler:
            result = dispatcher._get_kombu_consumer_kwargs(
                consumer=mock.Mock(), listeners=[listener]
            )

        _wrap_handler.assert_called_once_with(handler)

        self.assertEqual(1, len(result[0]["callbacks"]))

        callback = result[0]["callbacks"][0]

        self.assertEqual(wrapped, callback)

    def test__get_kombu_consumer_kwargs_sets_auto_declare(self) -> None:
        dispatcher = kombu.Dispatcher()

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[mock.Mock(handlers=[], topics=[])]
        )

        self.assertTrue(result[0]["auto_declare"])

    def test__get_kombu_consumer_kwargs_sets_no_ack(self) -> None:
        dispatcher = kombu.Dispatcher()

        result = dispatcher._get_kombu_consumer_kwargs(
            consumer=mock.Mock(), listeners=[mock.Mock(handlers=[], topics=[])]
        )

        self.assertTrue(result[0]["no_ack"])

    def test__wrap_handler_does_not_call_function(self) -> None:
        dispatcher = kombu.Dispatcher()
        function = mock.Mock()

        dispatcher._wrap_handler(function)

        function.assert_not_called()

    def test__wrap_handler_wrapper_calls_get_metadata(self) -> None:
        dispatcher = kombu.Dispatcher()
        function = mock.Mock()
        wrapped = dispatcher._wrap_handler(function)

        body = "TEST_JSON"
        message = mock.Mock()

        with mock.patch(
            "clink.backends.kombu.Dispatcher.get_metadata"
        ) as get_metadata:
            wrapped(body, message=message)

        get_metadata.assert_called_once_with(body, message=message)

    def test__wrap_handler_wrapper_calls_function_with_body_and_metadata(
        self,
    ) -> None:
        dispatcher = kombu.Dispatcher()
        function = mock.Mock()
        wrapped = dispatcher._wrap_handler(function)

        body = "TEST_JSON"
        metadata = mock.Mock()

        with mock.patch(
            "clink.backends.kombu.Dispatcher.get_metadata",
            return_value=metadata,
        ):
            wrapped(body, message=mock.Mock())

        function.assert_called_once_with(body, metadata=metadata)

    @mock.patch("clink.backends.kombu.Dispatcher._get_kombu_consumer_kwargs")
    def test_consume_calls_kombu_consumerproducer_run(
        self, *args: mock.Mock
    ) -> None:
        instance = kombu.Dispatcher()
        with mock.patch(
            "clink.backends.kombu.KombuConsumerProducer.run"
        ) as run:
            instance.consume(consumer=mock.Mock())

        run.assert_called_once_with()

    @mock.patch("clink.backends.kombu.KombuConsumerProducer.run")
    def test_consume_calls__get_kombu_consumer_kwargs(
        self, *args: mock.Mock
    ) -> None:
        instance = kombu.Dispatcher()
        consumer = mock.Mock()

        with mock.patch(
            "clink.backends.kombu.Dispatcher._get_kombu_consumer_kwargs"
        ) as _get_kombu_consumer_kwargs:
            instance.consume(consumer=consumer)

        _get_kombu_consumer_kwargs.assert_called_once_with(
            consumer=consumer, listeners=consumer.listeners
        )

    @mock.patch("clink.backends.kombu.Dispatcher._get_kombu_consumer_kwargs")
    @mock.patch(
        "clink.backends.kombu.KombuConsumerProducer.run",
        side_effect=KeyboardInterrupt(),
    )
    def test_consume_logs_warning_on_keyboard_interrupt(
        self, *args: mock.Mock
    ) -> None:
        instance = kombu.Dispatcher()

        with self.assertLogs(level=logging.WARNING) as context:
            instance.consume(consumer=mock.Mock())

        self.assertIn("Keyboard interrupt", context.output[0])

    def test_emit_event_calls_backend_publish(self) -> None:
        event = mock.Mock(
            to_json=mock.Mock(return_value="TEST_EVENT_JSON"),
            type="TEST_EVENT_TYPE",
        )
        instance = kombu.Dispatcher()

        with mock.patch("clink.backends.kombu.Backend.publish") as publish:
            instance.emit_event(producer=mock.Mock(), event=event)

        publish.assert_called_once_with(
            body="TEST_EVENT_JSON",
            content_type="application/cloudevents+json",
            routing_key="TEST_EVENT_TYPE",
        )

    def test_get_metadata_returns_content_encoding(self) -> None:
        message = mock.Mock(content_type="TEST_CONTENT_ENCODING")

        result = kombu.Dispatcher().get_metadata(
            body="TEST_JSON", message=message
        )

        self.assertEqual(message.content_encoding, result["content_encoding"])

    def test_get_metadata_returns_content_type(self) -> None:
        message = mock.Mock(content_type="TEST_CONTENT_TYPE")

        result = kombu.Dispatcher().get_metadata(
            body="TEST_JSON", message=message
        )

        self.assertEqual(message.content_type, result["content_type"])

    def test_get_metadata_returns__message(self) -> None:
        message = mock.Mock()

        result = kombu.Dispatcher().get_metadata(
            body="TEST_JSON", message=message
        )

        self.assertEqual(message, result["_message"])

    def test_get_metadata_returns_message_headers(self) -> None:
        message = mock.Mock(headers={"TEST_KEY": "TEST_VALUE"})

        result = kombu.Dispatcher().get_metadata(
            body="TEST_JSON", message=message
        )

        self.assertEqual(message.headers, result["headers"])

    def test_run_command_raises_not_implemented_error(self) -> None:
        instance = kombu.Dispatcher()

        with self.assertRaises(NotImplementedError):
            instance.run_command(producer=mock.Mock(), command=mock.Mock())

    def test_send_message_raises_not_implemented_error(self) -> None:
        instance = kombu.Dispatcher()

        with self.assertRaises(NotImplementedError):
            instance.send_message(producer=mock.Mock(), message=mock.Mock())


class KombuConsumerProducerTestCase(unittest.TestCase):
    def test___init___sets_connection(self) -> None:
        value = mock.Mock()

        instance = kombu.KombuConsumerProducer(
            connection=value, exchange=mock.Mock()
        )

        self.assertEqual(value, instance.connection)

    def test___init___sets_exchange(self) -> None:
        value = mock.Mock()

        instance = kombu.KombuConsumerProducer(
            connection=mock.Mock(), exchange=value
        )

        self.assertEqual(value, instance.exchange)

    def test___init___sets_empty_consumer_kwargs_when_not_provided(
        self,
    ) -> None:
        instance = kombu.KombuConsumerProducer(
            connection=mock.Mock(), exchange=mock.Mock()
        )

        self.assertFalse(instance.consumer_kwargs)

    def test___init___sets_consumer_kwargs(self) -> None:
        value = mock.Mock()

        instance = kombu.KombuConsumerProducer(
            connection=mock.Mock(),
            exchange=mock.Mock(),
            consumer_kwargs=value,
        )

        self.assertEqual(value, instance.consumer_kwargs)

    def test_get_consumers_returns_list_of_consumer_instances_with_kwargs(
        self,
    ) -> None:
        instance = kombu.KombuConsumerProducer(
            connection=mock.Mock(),
            exchange=mock.Mock(),
            consumer_kwargs=[
                {"key_one": "VALUE_ONE", "key_two": "VALUE_TWO"},
                {"key_three": "VALUE_THREE", "key_four": "VALUE_FOUR"},
            ],
        )

        Consumer = mock.Mock()
        result = instance.get_consumers(Consumer=Consumer, channel=mock.Mock())

        self.assertIsInstance(result, list)
        self.assertEqual(2, len(result))

        call_one_kwargs = Consumer.call_args_list[0][1]
        call_two_kwargs = Consumer.call_args_list[1][1]

        self.assertEqual(
            call_one_kwargs, {"key_one": "VALUE_ONE", "key_two": "VALUE_TWO"}
        )
        self.assertEqual(
            call_two_kwargs,
            {"key_three": "VALUE_THREE", "key_four": "VALUE_FOUR"},
        )
