import logging
import unittest
import unittest.mock
from typing import Callable, List

import sentry_sdk

from clink import api as clink
from clink import handlers


class HandlerTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self._logger = logging.getLogger("clink.handlers")
        self._null_handler = logging.NullHandler()
        self._logger.addHandler(self._null_handler)

    def tearDown(self) -> None:
        self._logger.removeHandler(self._null_handler)

    def test___call___adds_handler_property(self) -> None:
        function = unittest.mock.Mock()
        handler = handlers.Handler()
        decorated_function: Callable = handler(function)

        self.assertEqual(
            decorated_function._handler, handler  # type: ignore[attr-defined]
        )

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=False
    )
    def test_wrapper_calls_parse_body(self, *args: unittest.mock.Mock) -> None:
        function = unittest.mock.Mock()
        handler = handlers.Handler()
        decorated_function: Callable = handler(function)
        body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()

        with unittest.mock.patch.object(handler, "parse_body") as parse_body:
            decorated_function(body, metadata=metadata)

        parse_body.assert_called_once_with(body, metadata=metadata)

    def test_wrapper_calls_is_dry_run(self) -> None:
        function = unittest.mock.Mock()
        handler = handlers.Handler()
        decorated_function: Callable = handler(function)
        body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()

        with unittest.mock.patch.object(
            handler, "is_dry_run", return_value=False
        ) as is_dry_run:
            decorated_function(body, metadata=metadata)

        is_dry_run.assert_called_once_with(body, metadata=metadata)

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=False
    )
    def test_wrapper_calls_function_when_dry_run_is_false(
        self, *args: unittest.mock.Mock
    ) -> None:
        function = unittest.mock.Mock()
        decorated_function: Callable = handlers.Handler()(function)
        body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()

        decorated_function(body, metadata=metadata)

        function.assert_called_once_with(
            body, metadata=metadata, dry_run=False
        )

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=False
    )
    def test_wrapper_calls__configure_sentry_scope(
        self, *args: unittest.mock.Mock
    ) -> None:
        function = unittest.mock.Mock()
        decorated_function: Callable = handlers.Handler()(function)
        parsed_body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()
        scope = unittest.mock.Mock()

        with unittest.mock.patch(
            "clink.handlers.Handler.parse_body", return_value=parsed_body
        ), unittest.mock.patch(
            "sentry_sdk.configure_scope",
            return_value=unittest.mock.MagicMock(
                __enter__=unittest.mock.Mock(return_value=scope),
            ),
        ), unittest.mock.patch(
            "clink.handlers.Handler._configure_sentry_scope"
        ) as configure_sentry_scope:
            decorated_function(unittest.mock.Mock(), metadata=metadata)

        configure_sentry_scope.assert_called_once_with(
            scope,
            parsed_body,
            function=function,
            metadata=metadata,
            dry_run=False,
        )

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=True
    )
    def test_wrapper_calls_function_when_dry_run_is_true_and_supports_dry_run_is_true(  # noqa: E501
        self, *args: unittest.mock.Mock
    ) -> None:
        function = unittest.mock.Mock()
        decorated_function: Callable = handlers.Handler(supports_dry_run=True)(
            function
        )
        body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()

        decorated_function(body, metadata=metadata)

        function.assert_called_once_with(body, metadata=metadata, dry_run=True)

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=True
    )
    def test_wrapper_does_not_call_function_when_dry_run_is_true_and_supports_dry_run_is_false(  # noqa: E501
        self, *args: unittest.mock.Mock
    ) -> None:
        function = unittest.mock.Mock()
        decorated_function: Callable = handlers.Handler(
            supports_dry_run=False
        )(function)

        decorated_function(unittest.mock.Mock(), metadata=unittest.mock.Mock())

        function.assert_not_called()

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=True
    )
    def test_wrapper_logs_warning_when_dry_run_is_true_and_supports_dry_run_is_false(  # noqa: E501
        self, *args: unittest.mock.Mock
    ) -> None:
        function = unittest.mock.Mock()
        decorated_function: Callable = handlers.Handler(
            supports_dry_run=False
        )(function)

        with self.assertLogs(level=logging.WARNING) as context:
            decorated_function(
                unittest.mock.Mock(), metadata=unittest.mock.Mock()
            )

        self.assertIn(
            "Dry-run is not supported by this handler. Skipping.",
            context.output[-1],
        )

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=False
    )
    def test_wrapper_raises_exception_in_function_when_suppress_exceptions_is_false(  # noqa: E501
        self, *args: unittest.mock.Mock
    ) -> None:
        class TestException(Exception):
            pass

        function = unittest.mock.Mock(
            side_effect=TestException("TEST_FUNCTION_EXCEPTION")
        )
        decorated_function: Callable = handlers.Handler(
            suppress_exceptions=False
        )(function)
        body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()

        with self.assertRaises(TestException) as context:
            decorated_function(body, metadata=metadata)

        self.assertIn("TEST_FUNCTION_EXCEPTION", str(context.exception))

    @unittest.mock.patch(
        "clink.handlers.Handler.is_dry_run", return_value=False
    )
    def test_wrapper_logs_exception_in_function_when_suppress_exceptions_is_true(  # noqa: E501
        self, *args: unittest.mock.Mock
    ) -> None:
        class TestException(Exception):
            pass

        function = unittest.mock.Mock(
            side_effect=TestException("TEST_FUNCTION_EXCEPTION")
        )
        decorated_function: Callable = handlers.Handler(
            suppress_exceptions=True
        )(function)
        body = unittest.mock.Mock()
        metadata = unittest.mock.Mock()

        with self.assertLogs(
            "clink.handlers", level=logging.ERROR
        ) as logging_context:
            decorated_function(body, metadata=metadata)

        self.assertIn(
            'Unhandled exception "TEST_FUNCTION_EXCEPTION" was suppressed in '
            f'"{function}"',
            logging_context.output[-1],
        )
        self.assertEqual(logging.ERROR, logging_context.records[-1].levelno)

    def test___init___sets_dispatcher(self) -> None:
        dispatcher = unittest.mock.Mock()

        handler = handlers.Handler(dispatcher=dispatcher)

        self.assertEqual(dispatcher, handler.dispatcher)

    def test___init___sets_suppress_exceptions_when_false(self) -> None:
        handler = handlers.Handler(suppress_exceptions=False)

        self.assertFalse(handler.suppress_exceptions)

    def test___init___sets_suppress_exceptions_when_true(self) -> None:
        handler = handlers.Handler(suppress_exceptions=True)

        self.assertTrue(handler.suppress_exceptions)

    def test___init___does_not_set_suppress_exceptions_when_none(self) -> None:
        handler = handlers.Handler(suppress_exceptions=None)

        self.assertIsNotNone(handler.suppress_exceptions)
        self.assertFalse(handler.suppress_exceptions)

    def test___init___sets_supports_dry_run_when_false(self) -> None:
        handler = handlers.Handler(supports_dry_run=False)

        self.assertFalse(handler.supports_dry_run)

    def test___init___sets_supports_dry_run_when_true(self) -> None:
        handler = handlers.Handler(supports_dry_run=True)

        self.assertTrue(handler.supports_dry_run)

    def test___init___does_not_set_supports_dry_run_when_none(self) -> None:
        handler = handlers.Handler(supports_dry_run=None)

        self.assertIsNotNone(handler.supports_dry_run)
        self.assertFalse(handler.supports_dry_run)

    def test__configure_sentry_scope_sets_clink_handler_function_tag(
        self,
    ) -> None:
        handler = handlers.Handler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        handler._configure_sentry_scope(
            scope,
            None,
            function=HandlerTestCase.test__configure_sentry_scope_sets_clink_handler_function_tag,  # noqa: E501
            metadata=None,
            dry_run=True,
        )

        self.assertIn(
            unittest.mock.call(
                "clink_handler_function",
                "tests.test_handlers.HandlerTestCase.test__configure_sentry_scope_sets_clink_handler_function_tag",  # noqa: E501
            ),
            scope.set_tag.call_args_list,
        )

    def test__configure_sentry_scope_sets_dry_run_tag(self) -> None:
        handler = handlers.Handler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        handler._configure_sentry_scope(
            scope,
            None,
            function=HandlerTestCase.test__configure_sentry_scope_sets_dry_run_tag,  # noqa: E501
            metadata=None,
            dry_run=True,
        )

        self.assertIn(
            unittest.mock.call("clink_dry_run", True),
            scope.set_tag.call_args_list,
        )

    def test_is_dry_run_raises_not_implemented_error(self) -> None:
        handler = handlers.Handler()

        with self.assertRaises(NotImplementedError):
            handler.is_dry_run(unittest.mock.Mock())

    def test_parse_body_returns_body(self) -> None:
        handler = handlers.Handler()
        body = unittest.mock.Mock()

        result = handler.parse_body(body)

        self.assertEqual(body, result)

    def test_run_raises_not_implemented_error(self) -> None:
        handler = handlers.Handler()

        with self.assertRaises(NotImplementedError):
            handler.run.__wrapped__(unittest.mock.Mock())  # type: ignore[attr-defined]  # noqa: E501


class EventHandlerTestCase(unittest.TestCase):
    def test__configure_sentry_scope_sets_event_context(self) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(source="TEST_SOURCE", type="TEST_TYPE")

        handler._configure_sentry_scope(
            scope,
            event,
            function=unittest.mock.Mock(),
            metadata=unittest.mock.Mock(),
            dry_run=unittest.mock.Mock(),
        )

        scope.set_context.assert_called_once_with(
            "CLINK Event", event.to_dict()
        )

    def test__configure_sentry_scope_sets_event_source_tag(self) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(source="TEST_SOURCE", type="TEST_TYPE")

        handler._configure_sentry_scope(
            scope,
            event,
            function=unittest.mock.Mock(),
            metadata=unittest.mock.Mock(),
            dry_run=unittest.mock.Mock(),
        )

        self.assertIn(
            unittest.mock.call("clink_event.source", "TEST_SOURCE"),
            scope.set_tag.call_args_list,
        )

    def test__configure_sentry_scope_sets_event_type_tag(self) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(source="TEST_SOURCE", type="TEST_TYPE")

        handler._configure_sentry_scope(
            scope,
            event,
            function=unittest.mock.Mock(),
            metadata=unittest.mock.Mock(),
            dry_run=unittest.mock.Mock(),
        )

        self.assertIn(
            unittest.mock.call("clink_event.type", "TEST_TYPE"),
            scope.set_tag.call_args_list,
        )

    def test__configure_sentry_scope_sets_trace_id_tag(self) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(source="TEST_SOURCE", type="TEST_TYPE")

        handler._configure_sentry_scope(
            scope,
            event,
            function=unittest.mock.Mock(),
            metadata=unittest.mock.Mock(),
            dry_run=unittest.mock.Mock(),
        )

        self.assertIn(
            unittest.mock.call("clink_trace_id", event.trace_parent.trace_id),
            scope.set_tag.call_args_list,
        )

    def test__configure_sentry_scope_does_not_call_set_scope_tags_when_no_subject(  # noqa: E501
        self,
    ) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(
            source="TEST_SOURCE", type="TEST_TYPE", subject=None
        )

        with unittest.mock.patch(
            "clink.utils.set_scope_tags"
        ) as set_scope_tags:
            handler._configure_sentry_scope(
                scope,
                event,
                function=unittest.mock.Mock(),
                metadata=unittest.mock.Mock(),
                dry_run=unittest.mock.Mock(),
            )

        set_scope_tags.assert_not_called()

    def test__configure_sentry_scope_does_not_call_set_scope_tags_when_subject_does_not_match_to_urn(  # noqa: E501
        self,
    ) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(
            source="TEST_SOURCE", type="TEST_TYPE", subject="TEST_SUBJECT"
        )

        with unittest.mock.patch(
            "clink.utils.set_scope_tags"
        ) as set_scope_tags:
            handler._configure_sentry_scope(
                scope,
                event,
                function=unittest.mock.Mock(),
                metadata=unittest.mock.Mock(),
                dry_run=unittest.mock.Mock(),
            )

        set_scope_tags.assert_not_called()

    def test__configure_sentry_scope_calls_set_scope_tags_when_subject_is_set(
        self,
    ) -> None:
        handler = handlers.EventHandler()
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)
        event = clink.Event(
            source="TEST_SOURCE",
            type="TEST_TYPE",
            subject="urn:TEST:SERVICE:REGION:ACCOUNT:TYPE/ID",
        )

        with unittest.mock.patch(
            "clink.utils.set_scope_tags"
        ) as set_scope_tags:
            handler._configure_sentry_scope(
                scope,
                event,
                function=unittest.mock.Mock(),
                metadata=unittest.mock.Mock(),
                dry_run=unittest.mock.Mock(),
            )

        set_scope_tags.assert_called_once_with(
            scope,
            tags={
                "clink_event.subject": "urn:TEST:SERVICE:REGION:ACCOUNT:TYPE/ID",  # noqa: E501
                "clink_event.subject.partition": "TEST",
                "clink_event.subject.service": "SERVICE",
                "clink_event.subject.region": "REGION",
                "clink_event.subject.account": "ACCOUNT",
                "clink_event.subject.resourcetype": "TYPE",
                "clink_event.subject.resourceid": "ID",
            },
        )

    def test_is_dry_run_returns_body_dry_run(self) -> None:
        handler = handlers.EventHandler(dispatcher=unittest.mock.Mock())
        body = unittest.mock.Mock(dry_run="TEST_DRY_RUN_VALUE")

        result = handler.is_dry_run(body=body)

        self.assertEqual("TEST_DRY_RUN_VALUE", result)

    def test_parse_body_logs_info(self) -> None:
        handler = handlers.EventHandler(dispatcher=unittest.mock.Mock())

        event = unittest.mock.Mock(
            type="TEST_TYPE",
            id="TEST_ID",
            source="TEST_SOURCE",
            subject="TEST_SUBJECT",
            trace_parent=unittest.mock.Mock(
                trace_id="TEST_TRACE_ID", parent_id="TEST_PARENT_ID"
            ),
        )

        with unittest.mock.patch(
            "clink.messaging.Event.from_json", return_value=event
        ):
            with self.assertLogs(level=logging.INFO) as context:
                handler.parse_body(
                    body=unittest.mock.Mock(),
                    metadata={"content_type": "application/cloudevents+json"},
                )

        self.assertIn(
            'Received "TEST_TYPE" event from "TEST_SOURCE" for "TEST_SUBJECT"',
            context.output[-1],
        )
        self.assertIn(event.id, context.output[-1])
        self.assertIn(event.trace_parent.trace_id, context.output[-1])
        self.assertIn(event.trace_parent.parent_id, context.output[-1])

    def test_parse_body_returns_result_of_event_from_json(self) -> None:
        handler = handlers.EventHandler(dispatcher=unittest.mock.Mock())

        body = unittest.mock.Mock()
        event = unittest.mock.Mock()

        with unittest.mock.patch(
            "clink.messaging.events.Event.from_json", return_value=event
        ) as from_json:
            result = handler.parse_body(
                body=body,
                metadata={"content_type": "application/cloudevents+json"},
            )

        from_json.assert_called_once_with(
            body, metadata={"content_type": "application/cloudevents+json"}
        )
        self.assertEqual(event, result)

    @unittest.mock.patch(
        "clink.messaging.events.EventSchema.loads",
        return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
    )
    def test_parse_body_sets_event__metadata(
        self, *args: unittest.mock.Mock
    ) -> None:
        handler = handlers.EventHandler(dispatcher=unittest.mock.Mock())

        body = unittest.mock.Mock()
        metadata = {
            "content_type": "application/cloudevents+json",
            "TEST_KEY": "TEST_VALUE",
        }

        event = handler.parse_body(body=body, metadata=metadata)

        self.assertEqual(event._metadata, metadata)

    def test_parse_body_raises_valueerror_for_message_with_wrong_content_type(
        self,
    ) -> None:
        handler = handlers.EventHandler(dispatcher=unittest.mock.Mock())

        with self.assertRaises(ValueError) as context:
            handler.parse_body(
                body=unittest.mock.Mock(),
                metadata={"content_type": "TEST_UNKNOWN"},
            )

        self.assertIn(
            'Expected content type of "application/cloudevents+json", but '
            'received "TEST_UNKNOWN"',
            str(context.exception),
        )

    def test_run_raises_not_implemented_error(self) -> None:
        handler = handlers.EventHandler()

        with self.assertRaises(NotImplementedError):
            handler.run.__wrapped__(unittest.mock.Mock())  # type: ignore[attr-defined]  # noqa: E501


class HandlerDecoratorTestCase(unittest.TestCase):
    def test_calls_and_returns_base_with_dispatcher_and_options(self) -> None:
        base = unittest.mock.Mock()
        dispatcher = unittest.mock.Mock()
        supports_dry_run = unittest.mock.Mock()
        suppress_exceptions = unittest.mock.Mock()

        handlers.handler(
            base=base,
            dispatcher=dispatcher,
            suppress_exceptions=suppress_exceptions,
            supports_dry_run=supports_dry_run,
        )

        base.assert_called_once_with(
            dispatcher=dispatcher,
            suppress_exceptions=suppress_exceptions,
            supports_dry_run=supports_dry_run,
        )


class EventHandlerDecoratorTestCase(unittest.TestCase):
    def test_calls_and_returns_handler_with_base_as_eventhandler(self) -> None:
        with unittest.mock.patch(
            "clink.handlers.handler", return_value="TEST_RESULT"
        ) as handler:
            result = handlers.event_handler(
                test_one="TEST_ONE", test_two="TEST_TWO"
            )

        handler.assert_called_once_with(
            base=handlers.EventHandler,
            test_one="TEST_ONE",
            test_two="TEST_TWO",
        )
        self.assertEqual("TEST_RESULT", result)


class ParseHandlersReferencesTestCase(unittest.TestCase):
    def test_returns_list_of_one_callable_given_one_callable(self) -> None:
        value = unittest.mock.Mock()

        result: List[Callable] = handlers._parse_handler_references(
            handlers=value, context=None
        )

        self.assertIsInstance(result, list)
        self.assertEqual(result, [value])

    def test_returns_list_of_one_callable_given_list_of_one_callable(
        self,
    ) -> None:
        value = [unittest.mock.Mock()]

        result: List[Callable] = handlers._parse_handler_references(
            handlers=value, context=None
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)
        self.assertEqual(result, value)

    def test_returns_list_of_two_callables_given_list_of_two_callables(
        self,
    ) -> None:
        value = [unittest.mock.Mock(), unittest.mock.Mock()]

        result: List[Callable] = handlers._parse_handler_references(
            handlers=value, context=None
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertEqual(result, value)

    def test_returns_list_of_one_callable_given_one_string(self) -> None:
        context = unittest.mock.Mock()

        result: List[Callable] = handlers._parse_handler_references(
            handlers="handler", context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], context.handler)

    def test_returns_list_of_one_callable_given_list_of_one_string(
        self,
    ) -> None:
        context = unittest.mock.Mock()

        result: List[Callable] = handlers._parse_handler_references(
            handlers=["handler"], context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], context.handler)

    def test_returns_list_of_two_callables_given_list_of_two_strings(
        self,
    ) -> None:
        context = unittest.mock.Mock()

        result: List[Callable] = handlers._parse_handler_references(
            handlers=["handler_one", "handler_two"], context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], context.handler_one)
        self.assertEqual(result[1], context.handler_two)

    def test_returns_list_of_two_callables_given_list_of_one_string_and_one_callable(  # noqa: E501
        self,
    ) -> None:
        value = unittest.mock.Mock()
        context = unittest.mock.Mock()

        result: List[Callable] = handlers._parse_handler_references(
            handlers=[value, "handler_two"], context=context
        )

        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], value)
        self.assertEqual(result[1], context.handler_two)
