import decimal
import unittest
from typing import Any

from clink import handlers, utils


class TestHandler(handlers.EventHandler):
    def run(self, *args: Any, **kwargs: Any) -> None:
        pass


@handlers.event_handler()
def test_handler(*args: Any, **kwargs: Any) -> None:
    pass


class GetFullQualnameTestCase(unittest.TestCase):
    def test_returns_expected_value_for_str_class(self) -> None:
        result = utils.get_full_qualname(str)

        self.assertEqual("builtins.str", result)

    def test_returns_expected_value_for_str_instance(self) -> None:
        result = utils.get_full_qualname("HELLO")

        self.assertEqual("builtins.str", result)

    def test_returns_expected_value_for_decimal(self) -> None:
        result = utils.get_full_qualname(decimal.Decimal)

        self.assertEqual("decimal.Decimal", result)

    def test_returns_expected_value_for_decimal_instance(self) -> None:
        result = utils.get_full_qualname(decimal.Decimal("1234"))

        self.assertEqual("decimal.Decimal", result)

    def test_returns_expected_value_for_decimal_instance_isnan(self) -> None:
        result = utils.get_full_qualname(decimal.Decimal("1234").is_nan)

        self.assertEqual("decimal.Decimal.is_nan", result)

    def test_returns_expected_value_for_test_handler_class(self) -> None:
        result = utils.get_full_qualname(TestHandler)

        self.assertEqual("tests.utils.test___init__.TestHandler", result)

    def test_returns_expected_value_for_test_handler_instance(self) -> None:
        result = utils.get_full_qualname(TestHandler())

        self.assertEqual("tests.utils.test___init__.TestHandler", result)

    def test_returns_expected_value_for_test_handler_run(self) -> None:
        result = utils.get_full_qualname(TestHandler().run)

        self.assertEqual("tests.utils.test___init__.TestHandler.run", result)

    def test_returns_expected_value_for_test_handler_function(self) -> None:
        result = utils.get_full_qualname(test_handler)

        self.assertEqual("tests.utils.test___init__.test_handler", result)
