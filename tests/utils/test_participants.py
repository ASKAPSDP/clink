import unittest
import unittest.mock
from typing import Any

from clink import api as clink
from clink.utils import participants


component = clink.Participant("test")


@component.on_event("test.a", "test.c")
def a(event: clink.Event, **kwargs: Any) -> None:
    pass


@component.on_event("test.b", "test.e")
def b(event: clink.Event, **kwargs: Any) -> None:
    pass


@component.on_event("test.d")
def c(event: clink.Event, **kwargs: Any) -> None:
    pass


@component.on_event("test.a", "test.c", name="aa")
def aa(event: clink.Event, **kwargs: Any) -> None:
    pass


@component.on_event("test.b", "test.e", name="bb")
def bb(event: clink.Event, **kwargs: Any) -> None:
    pass


@component.on_event("test.d", name="cc")
def cc(event: clink.Event, **kwargs: Any) -> None:
    pass


class ConsumerListenersTable(unittest.TestCase):
    maxDiff = 5000

    def test_returns_expected_text(self) -> None:

        result = participants.consumer_listeners_table(component)

        expected = (
            "Consuming...                                                          \n"  # noqa: E501
            "                                                                      \n"  # noqa: E501
            " Name      Topic        Handler                                       \n"  # noqa: E501
            " ──────────────────────────────────────────────────────────────────── \n"  # noqa: E501
            "           test.a       tests.utils.test_participants.a               \n"  # noqa: E501
            "           test.b       tests.utils.test_participants.b               \n"  # noqa: E501
            "           test.c       tests.utils.test_participants.a               \n"  # noqa: E501
            "           test.d       tests.utils.test_participants.c               \n"  # noqa: E501
            "           test.e       tests.utils.test_participants.b               \n"  # noqa: E501
            " aa        test.a       tests.utils.test_participants.aa              \n"  # noqa: E501
            " aa        test.c       tests.utils.test_participants.aa              \n"  # noqa: E501
            " bb        test.b       tests.utils.test_participants.bb              \n"  # noqa: E501
            " bb        test.e       tests.utils.test_participants.bb              \n"  # noqa: E501
            " cc        test.d       tests.utils.test_participants.cc              \n"  # noqa: E501
            "                                                                      "  # noqa: E501
        )

        self.assertEqual(str(result), expected)
