import unittest
import unittest.mock

import sentry_sdk

from clink.utils import sentry


class SetScopeTagsTestCase(unittest.TestCase):
    def test_does_not_call_set_tag_when_tags_is_empty_dict(self) -> None:
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        sentry.set_scope_tags(scope=scope, tags={})

        scope.set_tag.assert_not_called()

    def test_does_not_call_set_tag_when_tag_value_is_none(self) -> None:
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        sentry.set_scope_tags(scope, tags={"TEST_KEY": None})

        scope.set_tag.assert_not_called()

    def test_does_not_call_set_tag_when_tag_value_is_empty_string(
        self,
    ) -> None:
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        sentry.set_scope_tags(scope, tags={"TEST_KEY": ""})

        scope.set_tag.assert_not_called()

    def test_calls_set_tag_when_tag_value_is_provided(self) -> None:
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        sentry.set_scope_tags(scope, tags={"TEST_KEY": "TEST_VALUE"})

        scope.set_tag.assert_called_once_with("TEST_KEY", "TEST_VALUE")

    def test_calls_set_tag_when_tag_value_is_false(self) -> None:
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        sentry.set_scope_tags(scope, tags={"TEST_KEY": False})

        scope.set_tag.assert_called_once_with("TEST_KEY", False)

    def test_has_expected_calls_to_set_tag(self) -> None:
        scope = unittest.mock.Mock(spec=sentry_sdk.Scope)

        sentry.set_scope_tags(
            scope,
            tags={
                "TEST_KEY_ONE": False,
                "TEST_KEY_TWO": None,
                "TEST_KEY_THREE": "",
                "TEST_KEY_FOUR": [],
                "TEST_KEY_FIVE": "abcd",
                "TEST_KEY_SIX": 1234,
                "test.key.seven": True,
            },
        )

        self.assertEqual(4, scope.set_tag.call_count)

        self.assertEqual(
            scope.set_tag.call_args_list[0][0], ("TEST_KEY_ONE", False)
        )
        self.assertEqual(
            scope.set_tag.call_args_list[1][0], ("TEST_KEY_FIVE", "abcd")
        )
        self.assertEqual(
            scope.set_tag.call_args_list[2][0], ("TEST_KEY_SIX", 1234)
        )
        self.assertEqual(
            scope.set_tag.call_args_list[3][0], ("test.key.seven", True)
        )
