import unittest

from clink.utils import functional


class IsListTestCase(unittest.TestCase):
    def test_returns_true__when_obj_is_list(self) -> None:
        result = functional.is_list(list())

        self.assertTrue(result)

    def test_returns_false__when_obj_is_string(self) -> None:
        result = functional.is_list("test")

        self.assertFalse(result)


class MaybeListTestCase(unittest.TestCase):
    def test_returns_list_when_obj_is_string(self) -> None:
        result = functional.maybe_list("test")

        self.assertIsInstance(result, list)

    def test_returns_list_when_obj_is_list(self) -> None:
        result = functional.maybe_list([1, 2, 3, 4])

        self.assertIsInstance(result, list)
        self.assertEqual(result, [1, 2, 3, 4])
