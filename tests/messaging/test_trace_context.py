import logging
import unittest
from unittest import mock

from clink.messaging import trace_context


class TraceParentTestCase(unittest.TestCase):
    def test___str__returns_expected_header_value_format(self) -> None:
        instance = trace_context.TraceParent(
            trace_id="111222333444", parent_id="556677"
        )

        self.assertEqual("00-111222333444-556677-00000000", str(instance))

    def test_from_string_raises_valueerror_for_invalid_format(self) -> None:
        with self.assertRaises(ValueError) as context:
            trace_context.TraceParent.from_string("INVALID")

        self.assertIn(
            "Not a valid traceparent string format", str(context.exception)
        )

    def test_from_string_returns_traceparent_with_expected_attribute_values(
        self,
    ) -> None:
        instance = trace_context.TraceParent.from_string(
            "00-11112222333344445555666677778888-1122334455667788-00",
        )

        self.assertEqual("00", instance.version)
        self.assertEqual("11112222333344445555666677778888", instance.trace_id)
        self.assertEqual("1122334455667788", instance.parent_id)
        self.assertEqual("00", instance.trace_flags)

    def test_to_string_calls___str__(self) -> None:
        instance = trace_context.TraceParent()

        with mock.patch(
            "clink.messaging.trace_context.TraceParent.__str__",
            return_value="",
        ) as __str__:
            instance.to_string()

        __str__.assert_called_once_with()


class TraceStateTestCase(unittest.TestCase):
    def test___setitem__adds_item_to_end_when_updating_existing_item(
        self,
    ) -> None:
        instance = trace_context.TraceState(one="1", two="2", three="3")

        items = list(instance.items())
        first_key, first_value = items[0]
        last_key, last_value = items[-1]

        self.assertEqual("one", first_key)
        self.assertEqual("1", first_value)
        self.assertEqual("three", last_key)
        self.assertEqual("3", last_value)

        instance.update({"one": "1-updated"})

        items = list(instance.items())
        first_key, first_value = items[0]
        last_key, last_value = items[-1]

        self.assertEqual("two", first_key)
        self.assertEqual("2", first_value)
        self.assertEqual("one", last_key)
        self.assertEqual("1-updated", last_value)

        instance["two"] = "2-updated"

        items = list(instance.items())
        first_key, first_value = items[0]
        last_key, last_value = items[-1]

        self.assertEqual("three", first_key)
        self.assertEqual("3", first_value)
        self.assertEqual("two", last_key)
        self.assertEqual("2-updated", last_value)

    def test___str__returns_items_as_list_members(self) -> None:
        instance = trace_context.TraceState(
            test_key="TEST_VALUE", test_another_key=" TEST ANOTHER VALUE"
        )

        self.assertIn("test_key=TEST_VALUE", str(instance))
        self.assertIn("test_another_key= TEST ANOTHER VALUE", str(instance))

    def test___str__reverses_items(self) -> None:
        instance = trace_context.TraceState()
        instance.update({"zero": "0"})
        instance.update({"one": "1"})
        instance.update({"two": "2"})

        self.assertEqual("two=2,one=1,zero=0", str(instance))

    def test_from_string_logs_warning_for_invalid_list_member(self) -> None:
        with self.assertLogs(level=logging.WARNING) as context:
            trace_context.TraceState.from_string("INVALID")

        self.assertIn(
            '"INVALID" is not a valid tracestate string format',
            context.output[-1],
        )

    def test_from_string_returns_tracestate_dict(self) -> None:
        instance = trace_context.TraceState.from_string("key=value")

        self.assertIsInstance(instance, trace_context.TraceState)
        self.assertIn("key", instance)
        self.assertEqual("value", instance["key"])

    def test_from_string_removes_surrounding_whitespace(self) -> None:
        instance = trace_context.TraceState.from_string(
            " test_key=TEST_VALUE , test_another_key= TEST ANOTHER VALUE "
        )

        self.assertEqual("TEST_VALUE", instance["test_key"])
        self.assertEqual(" TEST ANOTHER VALUE", instance["test_another_key"])

    def test_from_string_returns_empty_tracestate_for_empty_string(
        self,
    ) -> None:
        instance = trace_context.TraceState.from_string("")

        self.assertEqual(0, len(instance.items()))

    def test_from_string_reverses_values(self) -> None:
        instance = trace_context.TraceState.from_string("zero=0,one=1,two=2")

        items = list(instance.items())
        self.assertEqual(("two", "2"), items[0])
        self.assertEqual(("one", "1"), items[1])
        self.assertEqual(("zero", "0"), items[2])

    def test_slugify_replaces_period_with_hyphen(self) -> None:
        result = trace_context.TraceState.slugify("clink")

        self.assertNotIn(".", result)
        self.assertEqual("clink", result)

    def test_slugify_converts_to_lower_case(self) -> None:
        result = trace_context.TraceState.slugify("  CLINK @TEST VALUE!")

        self.assertEqual("clink-@test-value", result)

    def test_to_string_calls___str__(self) -> None:
        instance = trace_context.TraceState()

        with mock.patch(
            "clink.messaging.trace_context.TraceState.__str__",
            return_value="",
        ) as __str__:
            instance.to_string()

        __str__.assert_called_once_with()
