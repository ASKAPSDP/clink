import unittest
from datetime import datetime
from unittest import mock

from freezegun import freeze_time

from clink.messaging import events


try:
    import zoneinfo
except ImportError:  # Python < 3.9
    from backports import zoneinfo


class GetUTCNowTestCase(unittest.TestCase):
    @freeze_time("2001-02-03")
    def test_calls_datetime_now_with_utc_zoneinfo(self) -> None:
        result = events._get_utc_now()

        self.assertEqual(result.tzinfo, zoneinfo.ZoneInfo("UTC"))
        self.assertEqual(result.year, 2001)
        self.assertEqual(result.month, 2)
        self.assertEqual(result.day, 3)


class CloudEventSchemaTestCase(unittest.TestCase):
    def test_to_trace_parent_calls_traceparent_from_string(self) -> None:
        with mock.patch(
            "clink.messaging.trace_context.TraceParent.from_string"
        ) as from_string:
            events.CloudEventSchema().to_trace_parent(
                {"trace_parent": "TEST_VALUE"}
            )

        from_string.assert_called_once_with("TEST_VALUE")

    def test_to_trace_parent_does_not_call_traceparent_from_string_if_not_in_data(  # noqa: E501
        self,
    ) -> None:
        with mock.patch(
            "clink.messaging.trace_context.TraceParent.from_string"
        ) as from_string:
            events.CloudEventSchema().to_trace_parent({})

        from_string.assert_not_called()

    def test_to_trace_parent_returns_traceparent_instance(self) -> None:
        data = {
            "trace_parent": "00-11112222333344445555666677778888-1122334455667788-00000000"  # noqa: E501
        }

        result = events.CloudEventSchema().to_trace_parent(data)

        self.assertIsInstance(result["trace_parent"], events.TraceParent)
        self.assertEqual("00", result["trace_parent"].version)
        self.assertEqual(
            "11112222333344445555666677778888", result["trace_parent"].trace_id
        )
        self.assertEqual("1122334455667788", result["trace_parent"].parent_id)
        self.assertEqual("00000000", result["trace_parent"].trace_flags)

    def test_to_trace_state_calls_tracestate_from_string(self) -> None:
        with mock.patch(
            "clink.messaging.trace_context.TraceState.from_string"
        ) as from_string:
            events.CloudEventSchema().to_trace_state(
                {"trace_state": "TEST_VALUE"}
            )

        from_string.assert_called_once_with("TEST_VALUE")

    def test_to_trace_state_does_not_call_tracestate_from_string_if_not_in_data(  # noqa: E501
        self,
    ) -> None:
        with mock.patch(
            "clink.messaging.trace_context.TraceState.from_string"
        ) as from_string:
            events.CloudEventSchema().to_trace_state({})

        from_string.assert_not_called()

    def test_to_trace_state_returns_tracestate_instance(self) -> None:
        data = {"trace_state": "key=value,one=1,two=two"}

        result = events.CloudEventSchema().to_trace_state(data)

        self.assertIsInstance(result["trace_state"], events.TraceState)
        self.assertEqual("value", result["trace_state"]["key"])
        self.assertEqual("1", result["trace_state"]["one"])
        self.assertEqual("two", result["trace_state"]["two"])

    def test_to_utc_updates_time_to_utc(self) -> None:
        data = {
            "key": "value",
            "time": datetime(
                2020,
                6,
                7,
                10,
                20,
                tzinfo=zoneinfo.ZoneInfo("Australia/Lord_Howe"),
            ),
        }

        result = events.CloudEventSchema().to_utc(data)

        self.assertEqual(result["key"], "value")
        self.assertEqual(result["time"].tzinfo, zoneinfo.ZoneInfo("UTC"))
        self.assertEqual(result["time"].year, 2020)
        self.assertEqual(result["time"].month, 6)
        self.assertEqual(result["time"].day, 6)
        self.assertEqual(result["time"].hour, 23)
        self.assertEqual(result["time"].minute, 50)

    def test_to_utc_returns_data(self) -> None:
        data = {"key": "value"}

        result = events.CloudEventSchema().to_utc(data)

        self.assertEqual(result, data)

    def test_remove_none_values(self) -> None:
        data = {"none_value": None, "other_value": "value"}

        result = events.CloudEventSchema().remove_none_values(data)

        self.assertNotIn("none_value", result)
        self.assertIn("other_value", result)


class CloudEventTestCase(unittest.TestCase):
    def test_from_dict_load_from_schema_with_data(self) -> None:
        data = mock.Mock()

        with mock.patch(
            "clink.messaging.events.CloudEventSchema.load",
            return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
        ) as schema_load:
            events.CloudEvent.from_dict(data)

        schema_load.assert_called_once_with(data)

    @mock.patch(
        "clink.messaging.events.CloudEventSchema.load",
        return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
    )
    def test_from_dict_sets_metadata(self, *args: mock.Mock) -> None:
        metadata = mock.Mock()

        event = events.CloudEvent.from_dict(mock.Mock(), metadata=metadata)

        self.assertEqual(event._metadata, metadata)

    def test_from_json_loads_from_schema_with_data(self) -> None:
        data = mock.Mock()

        with mock.patch(
            "clink.messaging.events.CloudEventSchema.loads",
            return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
        ) as schema_loads:
            events.CloudEvent.from_json(data)

        schema_loads.assert_called_once_with(data)

    @mock.patch(
        "clink.messaging.events.CloudEventSchema.loads",
        return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
    )
    def test_from_json_sets_metadata(self, *args: mock.Mock) -> None:
        metadata = mock.Mock()

        event = events.CloudEvent.from_json(mock.Mock(), metadata=metadata)

        self.assertEqual(event._metadata, metadata)

    def test_sets_data_content_type_when_provided(self) -> None:
        instance = events.CloudEvent(
            source="TEST",
            type="TEST",
            data_content_type="TEST_DATA_CONTENT_TYPE",
        )

        self.assertEqual(instance.data_content_type, "TEST_DATA_CONTENT_TYPE")

    def test_does_not_set_data_content_type_to_application_json_when_data_and_data_content_type_is_not_provided(  # noqa: E501
        self,
    ) -> None:
        instance = events.CloudEvent(source="TEST", type="TEST")

        self.assertIsNone(instance.data_content_type)

    def test_sets_data_content_type_to_application_json_when_data_is_set_and_data_content_type_is_not_provided(  # noqa: E501
        self,
    ) -> None:
        instance = events.CloudEvent(source="TEST", type="TEST", data={})

        self.assertEqual(instance.data_content_type, "application/json")

    def test_sets_data_content_type_when_data_is_set_and_data_content_type_is_provided(  # noqa: E501
        self,
    ) -> None:
        instance = events.CloudEvent(
            source="TEST",
            type="TEST",
            data={},
            data_content_type="TEST_DATA_CONTENT_TYPE",
        )

        self.assertEqual(instance.data_content_type, "TEST_DATA_CONTENT_TYPE")

    def test_to_json_calls_schema_loads_and_returns_event(self) -> None:
        value = "TEST_VALUE"

        with mock.patch(
            "clink.messaging.events.CloudEventSchema.loads",
            return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
        ) as schema_loads:
            result = events.CloudEvent.from_json(value)

        schema_loads.assert_called_once_with(value)
        self.assertIsInstance(result, events.CloudEvent)

    def test_to_dict_calls_schema_load_and_returns_event(self) -> None:
        value = {"TEST_KEY": "TEST_VALUE"}

        with mock.patch(
            "clink.messaging.events.CloudEventSchema.load",
            return_value={"source": "TEST_SOURCE", "type": "TEST_TYPE"},
        ) as schema_load:
            result = events.CloudEvent.from_dict(value)

        schema_load.assert_called_once_with(value)
        self.assertIsInstance(result, events.CloudEvent)

    def test_to_dict_returns_schema_dump(self) -> None:
        instance = events.CloudEvent(source="TEST", type="TEST")
        mock_value = {"test": "value"}

        with mock.patch(
            "clink.messaging.events.CloudEventSchema.dump",
            return_value=mock_value,
        ) as schema_dump:
            result = instance.to_dict()

        schema_dump.assert_called_once_with(instance)
        self.assertEqual(result, mock_value)

    def test_to_json_returns_schema_dumps(self) -> None:
        instance = events.CloudEvent(source="TEST", type="TEST")
        mock_value = "TEST"

        with mock.patch(
            "clink.messaging.events.CloudEventSchema.dumps",
            return_value=mock_value,
        ) as schema_dumps:
            result = instance.to_json()

        schema_dumps.assert_called_once_with(instance)
        self.assertEqual(result, mock_value)


class SubjectURNTestCase(unittest.TestCase):
    def test___str___returns_formatted_string(self) -> None:
        instance = events.SubjectURN(
            partition="TEST_PARTITION",
            service="TEST_SERVICE",
            region="TEST_REGION",
            account="TEST_ACCOUNT",
            resource=events.SubjectURN.Resource(
                type="TEST_TYPE", id="TEST_ID"
            ),
        )

        expected = (
            "urn:TEST_PARTITION:TEST_SERVICE:TEST_REGION:TEST_ACCOUNT:"
            "TEST_TYPE/TEST_ID"
        )

        self.assertEqual(expected, str(instance))

    def test___str___returns_formatted_string_when_some_values_are_not_set(
        self,
    ) -> None:
        instance = events.SubjectURN(partition="TEST_PARTITION")

        expected = "urn:TEST_PARTITION::::"

        self.assertEqual(expected, str(instance))

    def test___str___returns_formatted_string_when_resource_type_is_not_set(
        self,
    ) -> None:
        instance = events.SubjectURN(
            partition="TEST_PARTITION",
            resource=events.SubjectURN.Resource(id="TEST_ID"),
        )

        expected = "urn:TEST_PARTITION::::TEST_ID"

        self.assertEqual(expected, str(instance))

    def test_from_string(self) -> None:
        value = (
            "urn:TEST_PARTITION:TEST_SERVICE:TEST_REGION::"
            "TEST_TYPE/TEST_ID/1:2/3"
        )

        instance = events.SubjectURN.from_string(value)

        self.assertEqual("TEST_PARTITION", instance.partition)
        self.assertEqual("TEST_SERVICE", instance.service)
        self.assertEqual("TEST_REGION", instance.region)
        self.assertEqual("", instance.account)
        self.assertEqual("TEST_TYPE", instance.resource.type)
        self.assertEqual("TEST_ID/1:2/3", instance.resource.id)

        self.assertEqual(value, str(instance))

    def test_resource___str___returns_formatted_string(self) -> None:
        instance = events.SubjectURN.Resource(type="TEST_TYPE", id="TEST_ID")

        expected = "TEST_TYPE/TEST_ID"

        self.assertEqual(expected, str(instance))

    def test_resource___str___formatted_string_when_id_is_not_set(
        self,
    ) -> None:
        instance = events.SubjectURN.Resource(type="TEST_TYPE")

        expected = "TEST_TYPE/"

        self.assertEqual(expected, str(instance))

    def test_resource___str___formatted_string_when_type_is_not_set(
        self,
    ) -> None:
        instance = events.SubjectURN.Resource(id="TEST_ID")

        expected = "TEST_ID"

        self.assertEqual(expected, str(instance))

    def test_resource_from_string(self) -> None:
        value = "TEST_TYPE/TEST_ID/1:2/3"

        instance = events.SubjectURN.Resource.from_string(value)

        self.assertEqual("TEST_TYPE", instance.type)
        self.assertEqual("TEST_ID/1:2/3", instance.id)

        self.assertEqual(value, str(instance))

    def test_verify_raises_typeerror_when_passed_incorrect_instance_type(
        self,
    ) -> None:
        incorrect_instance = object()

        with self.assertRaises(TypeError) as context:
            events.SubjectURN.verify(
                incorrect_instance  # type: ignore[arg-type]
            )

        self.assertIn(
            "Expected SubjectURN or Event, not \"<class 'object'>\"",
            str(context.exception),
        )

    def test_verify_raises_valueerror_with_event_invalid_subject_urn(
        self,
    ) -> None:
        event = events.Event(source="TEST", type="TEST", subject="INVALID")

        with self.assertRaises(ValueError) as context:
            events.SubjectURN.verify(
                event,
                resource_type="TEST_RESOURCE_TYPE",
            )

        self.assertIn(
            'Expected a resource type of "TEST_RESOURCE_TYPE", but could not '
            'parse event subject urn "INVALID"',
            str(context.exception),
        )

    def test_verify_raises_valueerror_when_event_subject_urn_is_none(
        self,
    ) -> None:
        event = events.Event(source="TEST", type="TEST", subject=None)

        with self.assertRaises(ValueError) as context:
            events.SubjectURN.verify(event, resource_type="TEST_RESOURCE_TYPE")

        self.assertIn(
            'Expected a resource type of "TEST_RESOURCE_TYPE", but event '
            "subject was not set",
            str(context.exception),
        )

    def test_verify_raises_valueerror_when_resource_type_is_not_what_is_expected(  # noqa: E501
        self,
    ) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:resource"
        )

        with self.assertRaises(ValueError) as context:
            subject_urn.verify(resource_type="TEST_EXPECTED_RESOURCE_TYPE")

        self.assertIn(
            'Expected a resource type of "TEST_EXPECTED_RESOURCE_TYPE", not '
            '"resource"',
            str(context.exception),
        )

    def test_verify_raises_valueerror_when_event_subject_urn_resource_type_is_not_what_is_expected(  # noqa: E501
        self,
    ) -> None:
        event = events.Event(
            source="TEST",
            type="TEST",
            subject="urn:partition:service:region:account:resource",
        )

        with self.assertRaises(ValueError) as context:
            events.SubjectURN.verify(
                event, resource_type="TEST_EXPECTED_RESOURCE_TYPE"
            )

        self.assertIn(
            'Expected a resource type of "TEST_EXPECTED_RESOURCE_TYPE", not '
            '"resource"',
            str(context.exception),
        )

    def test_verify_raises_valueerror_when_subject_urn_resource_type_is_not_what_is_expected(  # noqa: E501
        self,
    ) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:resource"
        )

        with self.assertRaises(ValueError) as context:
            events.SubjectURN.verify(
                subject_urn, resource_type="TEST_EXPECTED_RESOURCE_TYPE"
            )

        self.assertIn(
            'Expected a resource type of "TEST_EXPECTED_RESOURCE_TYPE", not '
            '"resource"',
            str(context.exception),
        )

    def test_verify_returns_none_when_no_keyword_arguments(self) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:TEST_RESOURCE_TYPE"
        )

        result = subject_urn.verify()  # type: ignore[func-returns-value]

        self.assertIsNone(result)

    def test_verify_returns_none_when_event_and_no_keyword_arguments(
        self,
    ) -> None:
        event = events.Event(
            source="TEST",
            type="TEST",
            subject="urn:partition:service:region:account:TEST_RESOURCE_TYPE",
        )

        result = events.SubjectURN.verify(  # type: ignore[func-returns-value]
            event
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_subject_urn_no_keyword_arguments(
        self,
    ) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:TEST_RESOURCE_TYPE"
        )

        result = events.SubjectURN.verify(  # type: ignore[func-returns-value]
            subject_urn
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_resource_type_matches(self) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:TEST_RESOURCE_TYPE"
        )

        result = subject_urn.verify(  # type: ignore[func-returns-value]
            resource_type="TEST_RESOURCE_TYPE"
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_event_subject_urn_resource_type_matches(
        self,
    ) -> None:
        event = events.Event(
            source="TEST",
            type="TEST",
            subject="urn:partition:service:region:account:TEST_RESOURCE_TYPE",
        )

        result = events.SubjectURN.verify(  # type: ignore[func-returns-value]
            event, resource_type="TEST_RESOURCE_TYPE"
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_subject_urn_resource_type_matches(
        self,
    ) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:TEST_RESOURCE_TYPE"
        )

        result = events.SubjectURN.verify(  # type: ignore[func-returns-value]
            subject_urn, resource_type="TEST_RESOURCE_TYPE"
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_resource_type_matches_with_id(
        self,
    ) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:TEST_RESOURCE_TYPE/TEST_ID"
        )

        result = subject_urn.verify(  # type:ignore[func-returns-value]
            resource_type="TEST_RESOURCE_TYPE"
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_event_subject_urn_resource_type_matches_with_id(  # noqa: E501
        self,
    ) -> None:
        event = events.Event(
            source="TEST",
            type="TEST",
            subject="urn:partition:service:region:account:TEST_RESOURCE_TYPE/TEST_ID",  # noqa: E501
        )

        result = events.SubjectURN.verify(  # type: ignore[func-returns-value]
            event, resource_type="TEST_RESOURCE_TYPE"
        )

        self.assertIsNone(result)

    def test_verify_returns_none_when_subject_urn_resource_type_matches_with_id(  # noqa: E501
        self,
    ) -> None:
        subject_urn = events.SubjectURN.from_string(
            "urn:partition:service:region:account:TEST_RESOURCE_TYPE/TEST_ID"
        )

        result = events.SubjectURN.verify(  # type: ignore[func-returns-value]
            subject_urn, resource_type="TEST_RESOURCE_TYPE"
        )

        self.assertIsNone(result)


class BaseEventTestCase(unittest.TestCase):
    def test_from_preceding_event_constructs_traceparent_from_preceding_event_with_same_trace_id(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE", type="TEST_ONE"
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event, source="TEST_SOURCE", type="TEST_TWO"
        )

        self.assertEqual(
            preceding_event.trace_parent.trace_id, event.trace_parent.trace_id
        )

    def test_from_preceding_event_constructs_tracestate_from_preceding_event_and_adds_new_event_parent_id(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE_1", type="TEST_ONE"
        )

        self.assertIn(
            preceding_event.trace_parent.parent_id,
            str(preceding_event.trace_state),
        )
        self.assertEqual(
            f"test_source_1={preceding_event.trace_parent.parent_id}",
            str(preceding_event.trace_state),
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event, source="TEST_SOURCE_2", type="TEST_TWO"
        )

        self.assertIn(event.trace_parent.parent_id, str(event.trace_state))
        self.assertIn(
            preceding_event.trace_parent.parent_id, str(event.trace_state)
        )
        self.assertEqual(
            f"test_source_2={event.trace_parent.parent_id},"
            f"test_source_1={preceding_event.trace_parent.parent_id}",
            str(event.trace_state),
        )

    def test_from_preceding_event_constructs_tracestate_without_preceding_event_parent_id_when_no_tracestate(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE_1", type="TEST_ONE"
        )

        preceding_event.trace_state = None

        self.assertNotIn(
            preceding_event.trace_parent.parent_id,
            str(preceding_event.trace_state),
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event, source="TEST_SOURCE_2", type="TEST_TWO"
        )

        self.assertIn(event.trace_parent.parent_id, str(event.trace_state))
        self.assertNotIn(
            preceding_event.trace_parent.parent_id, str(event.trace_state)
        )
        self.assertEqual(
            f"test_source_2={event.trace_parent.parent_id}",
            str(event.trace_state),
        )

    def test_from_preceding_event_does_not_set_dry_run_true_when_preceding_event_is_not_dry_run_and_not_explicitly_set(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE_1", type="TEST_TYPE_ONE", dry_run=False
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event, source="TEST_SOURCE_2", type="TEST_TYPE_2"
        )

        self.assertFalse(preceding_event.dry_run)
        self.assertFalse(event.dry_run)

    def test_from_preceding_event_sets_dry_run_true_when_preceding_event_is_dry_run_and_not_explicitly_set(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE_1", type="TEST_TYPE_ONE", dry_run=True
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event, source="TEST_SOURCE_2", type="TEST_TYPE_2"
        )

        self.assertTrue(preceding_event.dry_run)
        self.assertTrue(event.dry_run)

    def test_from_preceding_event_does_not_set_dry_run_true_when_preceding_event_is_dry_run_and_is_explicitly_set(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE_1", type="TEST_TYPE_ONE", dry_run=True
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event,
            source="TEST_SOURCE_2",
            type="TEST_TYPE_2",
            dry_run=False,
        )

        self.assertTrue(preceding_event.dry_run)
        self.assertFalse(event.dry_run)

    def test_from_preceding_event_sets_dry_run_true_when_preceding_event_is_not_dry_run_and_is_explicitly_set(  # noqa: E501
        self,
    ) -> None:
        preceding_event = events.BaseEvent(
            source="TEST_SOURCE_1", type="TEST_TYPE_ONE", dry_run=False
        )

        event = events.BaseEvent.from_preceding_event(
            preceding_event,
            source="TEST_SOURCE_2",
            type="TEST_TYPE_2",
            dry_run=True,
        )

        self.assertFalse(preceding_event.dry_run)
        self.assertTrue(event.dry_run)

    def test_subject_urn_returns_none_when_subject_is_none(self) -> None:
        event = events.BaseEvent(
            source="TEST_SOURCE", type="TEST_TYPE", subject=None
        )

        with mock.patch(
            "clink.messaging.events.SubjectURN.from_string",
        ) as from_string:
            result = event.subject_urn

        from_string.assert_not_called()
        self.assertIsNone(result)

    def test_subject_urn_returns_none_when_subject_urn_from_string_raises_valuerror(  # NOQA: E501
        self,
    ) -> None:
        event = events.BaseEvent(
            source="TEST_SOURCE", type="TEST_TYPE", subject="TEST_SUBJECT"
        )

        with mock.patch(
            "clink.messaging.events.SubjectURN.from_string",
            side_effect=ValueError(),
        ) as from_string:
            result = event.subject_urn

        from_string.assert_called_once_with("TEST_SUBJECT")
        self.assertIsNone(result)

    def test_subject_urn_returns_subjecturn_from_string(self) -> None:
        event = events.BaseEvent(
            source="TEST_SOURCE", type="TEST_TYPE", subject="TEST_SUBJECT"
        )
        mock_subjecturn = mock.Mock()

        with mock.patch(
            "clink.messaging.events.SubjectURN.from_string",
            return_value=mock_subjecturn,
        ) as from_string:
            result = event.subject_urn

        from_string.assert_called_once_with("TEST_SUBJECT")
        self.assertEqual(mock_subjecturn, result)

    def test_setting_subject_urn_sets_subject_string(self) -> None:
        subject_urn = events.SubjectURN(
            partition="TEST",
            service="SERVICE",
            resource=events.SubjectURN.Resource(type="TYPE", id="ID"),
        )

        event = events.BaseEvent(
            source="TEST_SOURCE", type="TEST_TYPE", subject=None
        )

        self.assertIsNone(event.subject)

        event.subject_urn = subject_urn

        self.assertEqual("urn:TEST:SERVICE:::TYPE/ID", event.subject)
