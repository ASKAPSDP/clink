# syntax=docker/dockerfile:1
#
# Build with podman or docker:
# podman build --build-arg uid=${UID} --build-arg gid=${GROUPS} --tag=clink .
# docker buildx build --file Containerfile --build-arg uid=${UID} --build-arg gid=${GROUPS} --tag=clink .
#
# Run with podman or docker:
# podman run --userns=keep-id --user ${UID}:${GROUPS} --rm --tty --interactive --volume="${PWD}":/opt/project clink
# docker run --user ${UID}:${GROUPS} --rm --tty --interactive --volume="${PWD}":/opt/project clink


FROM docker.io/library/ubuntu:20.04 AS base


LABEL org.opencontainers.image.source="https://bitbucket.csiro.au/projects/ASKAPSDP/repos/clink/"
LABEL org.opencontainers.image.title="clink"
LABEL org.opencontainers.image.vendor="CSIRO"


# Install required os packages
ENV DEBIAN_FRONTEND=noninteractive
RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache
RUN --mount=type=cache,target=/var/cache/apt --mount=type=cache,target=/var/lib/apt apt-get update --quiet --yes && \
    apt-get install --quiet --yes build-essential ca-certificates git python3 python3-dev python3-venv software-properties-common && \
    apt-get install --quiet --yes libenchant-2-2

RUN --mount=type=cache,target=/var/cache/apt --mount=type=cache,target=/var/lib/apt add-apt-repository --yes --update ppa:deadsnakes/ppa && \
    apt-get install --quiet --yes python3.6 python3.6-dev python3.6-venv python3.7 python3.7-dev python3.7-venv python3.8 python3.8-dev python3.8-venv python3.9 python3.9-dev python3.9-venv python3.10 python3.10-dev python3.10-venv python3.11 python3.11-dev python3.11-venv python3.12 python3.12-dev python3.12-venv python3.13 python3.13-dev python3.13-venv

# uv
COPY --from=ghcr.io/astral-sh/uv:0.5.23 /uv /usr/local/bin/uv

# Set up python 3.6, 3.7, 3.8, 3.9, 3.10, 3.11, 3.12, and 3.13 venvs
ENV UV_LINK_MODE=copy
RUN --mount=type=cache,id=pip,target=/var/cache/pip /usr/bin/python3.6 -m venv /opt/venv-3.6
RUN --mount=type=cache,id=pip,target=/var/cache/pip /opt/venv-3.6/bin/python -m pip install --force --upgrade "pip>=21.1"
RUN --mount=type=cache,id=pip,target=/var/cache/pip /opt/venv-3.6/bin/python -m pip install --force --upgrade setuptools wheel
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.7  /opt/venv-3.7
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.8  /opt/venv-3.8
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.9  /opt/venv-3.9
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.10 /opt/venv-3.10
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.11 /opt/venv-3.11
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.12 /opt/venv-3.12
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv venv --python=python3.13 /opt/venv-3.13


FROM base

# Install required python packages
ADD ./pyproject.toml /opt/project/
WORKDIR /opt/project
ADD ./README.md /opt/project/
ADD ./setup.cfg /opt/project/
ADD ./src/clink/__init__.py /opt/project/src/clink/__init__.py
RUN --mount=type=cache,id=pip,target=/var/cache/pip /opt/venv-3.6/bin/python -m      pip install --cache-dir=/var/cache/pip         --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.7/bin/python  --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.8/bin/python  --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.9/bin/python  --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.10/bin/python --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.11/bin/python --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.12/bin/python --editable /opt/project[redis]
RUN --mount=type=cache,id=uv-cache,target=/var/cache/uv uv --cache-dir=/var/cache/uv pip install --python=/opt/venv-3.13/bin/python --editable /opt/project[redis]


# Add code
ADD ./src /opt/project/src
ADD ./tests /opt/project/tests


# Configure environment
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
RUN ln -s /opt/venv-3.9/bin/clink /usr/local/bin/clink


# Create user
ARG uid=1000
ARG gid=1000
RUN id --group ${gid} || groupadd group --gid=${gid}
RUN id --user ${uid} || useradd user --uid=${uid} --gid=${gid} --create-home


# Defaults
WORKDIR /opt/project
USER ${uid}:${gid}
ENTRYPOINT ["clink"]
CMD ["--help"]
