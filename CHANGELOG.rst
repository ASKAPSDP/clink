.. :changelog:

=======
History
=======


Releases
========

0.36.0 (unreleased)
-------------------


0.35.0 (2025-01-30)
-------------------

* Adds support for Python 3.12 and Python 3.13.


0.34.0 (2023-01-04)
-------------------

* Loosened library requirements (removed upper version caps).
* Test against Python 3.11.
* Updated ``SubjectURN.verify`` to raise ``TypeError`` instead of
  ``ValueError`` when provided neither a ``SubjectURN`` or ``Event`` instance.


0.33.0 (2022-10-12)
-------------------

* Updated Event.subject_urn to return None if subject cannot be parsed as URN
  [`AXA-2096 <https://jira.csiro.au/browse/AXA-2096>`_].
* Updated setup.cfg to resolve issue with Python 3.7.x, kombu 5.2.4, and
  importlib-metadata 5.0.0: https://github.com/celery/kombu/issues/1600.


0.32.2 (2022-07-26)
-------------------

* Corrected use of sentry tag name to maximum of 32 characters. Updated name of
  CLINK Event context. Added user id/name to sentry when run from CLI
  [`AXA-1944 <https://jira.csiro.au/browse/AXA-1944>`_].


0.32.1 (2022-07-25)
-------------------

* Updated handlers to only log exceptions when ``suppress_exceptions`` is true
  [`AXA-1950 <https://jira.csiro.au/browse/AXA-1950>`_].
* Added ``get_full_qualname`` utility (fixes some naming issues).
* Prefix sentry context/tags with "clink\_" to prevent potential clashes
  [`AXA-1944 <https://jira.csiro.au/browse/AXA-1944>`_].
* Corrected typo of ``event_hander`` to ``event_handler``.


0.32.0 (2022-07-25)
-------------------

* Handlers now can suppress unhandled exceptions with the
  ``suppress_exceptions`` option [`AXA-1950 <https://jira.csiro.au/browse/AXA-1950>`_].
* Handlers now add sentry tags and context
  [`AXA-1944 <https://jira.csiro.au/browse/AXA-1944>`_].
* Updated ``Consumer.consume`` to log a table of listeners/handlers for topics
  it will be consuming [`AXA-1943 <https://jira.csiro.au/browse/AXA-1943>`_].


0.31.0 (2022-05-30)
-------------------

* Added initial work on ``SubjectURN.verify`` method. Currently checks the
  resource type [`AXA-1697 <https://jira.csiro.au/browse/AXA-1697>`_].
* Added ``clink.clink`` module (alias to ``clink.api``).
* Added ``long_description_content_type = text/markdown`` to setup.cfg.


0.30.0 (2022-03-17)
-------------------

* Renamed project from ``askap-clink`` to ``clink``
  [`AXA-1640 <https://jira.csiro.au/browse/AXA-1640>`_].
* Updated project use to PEP 517 / PEP 518 build API (and tox isolated builds).
  Removed setup.py.
* Updated version of black used by tox.


0.10.0 (2022-02-03)
-------------------

* Updated some required libraries for documentation generation and type
  checking.
* Run safety check using the python 3.7 venv
  [`AXA-1556 <https://jira.csiro.au/browse/AXA-1556>`_].


0.9 (2021-10-12)
----------------

* Updated kombu backend publish method to ensure it also declares the exchange
  [`AXA-1388 <https://jira.csiro.au/browse/AXA-1388>`_].
* Updated some required libraries for documentation generation and type
  checking.


0.8 (2021-08-16)
----------------

* Renamed ``askap.clink.simple`` to ``askap.clink.api``.
* Added ``SubjectURN`` and ``Resource`` dataclasses for assistance with parsing
  / generating event subject URNs
  [`AXA-1261 <https://jira.csiro.au/browse/AXA-1261>`_].
* Added experimental ``async_consume`` method to Consumer, and
  ``async_emit_event``, ``async_run_command``, and ``async_send_message``
  methods to Producer [`AXA-1260 <https://jira.csiro.au/browse/AXA-1260>`_].
* Updated CI/fabfile configuration to run tox in parallel mode.


0.7 (2021-07-27)
----------------

* Added additional propagation/preference of dry-run switches from preceding
  events and as CLI switch
  [`AXA-1192 <https://jira.csiro.au/browse/AXA-1192>`_].


0.6 (2021-06-15)
----------------

* Added "dummy" backend (e.g. for use in other projects when testing).


0.5 (2021-06-10)
----------------

* Moved kombu's ConsumerProducerMixin out of Backend and in to separate class
  [`AXA-1050 <https://jira.csiro.au/browse/AXA-1050>`_].
* Updated setup.cfg/tox for switch to modular typeshed with mypy 0.900 (adds
  [types] install option).


0.4.dev (2021-06-01)
--------------------

* Added py.typed to package data for PEP 561 -- Distributing and Packaging Type
  Information compatibility.


0.3.dev (2021-05-25)
--------------------

* CLI "consume" and "emit-event" utilities
  [`AXA-1075 <https://jira.csiro.au/browse/AXA-1075>`_].
* Additional logging.
* Added [tzdata] install option.


0.2.dev (2021-05-20)
--------------------

* Loosened library requirements.
* Test against Python 3.10.


0.1.dev (2021-05-20)
--------------------

* Initial development release.
